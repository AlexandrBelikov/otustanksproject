#! /bin/bash

shopt -s extglob
trash -v !(*.sh)
trash ./.cmake
trash ../src/generated_lib
cd ../autogenerate
./clean.sh