#ifndef {class_name_capitalized}_ADAPTER_H
#define {class_name_capitalized}_ADAPTER_H

#include "{desc.m_fileName}"
#include "ioc.h"
#include "uobject.h"
{desc.m_includes}
class {desc.m_className} : public {desc.m_interfaceName}
{{
public:
    {desc.m_className}(UObject& obj) :
        m_obj(obj)
    {{}}

{desc.m_publicMethods}
private:
    UObject& m_obj;
{desc.m_privateMethods}
}};

#endif // {class_name_capitalized}_ADAPTER_H