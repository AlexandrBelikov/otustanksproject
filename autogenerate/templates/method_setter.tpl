    {method.m_type} {method.m_name}({method.m_arguments}) {method.m_postfix}
    {{
        IoC::Resolve<p_lambda_t>("{className}.{method.m_name}", {{ std::ref(m_obj), {method.m_argNames} }})->Execute();
    }}

