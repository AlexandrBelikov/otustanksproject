#include "ioc.h"
{includes}
// Must be done once on app startup!
void register_generated_deps()
{{
    IoC::Resolve<p_lambda_t>(IOC::Register, {{ IOC::Adapter,
        scope_strategy_t([](const ioc_args_t& args)->std::any
        {{
            validate_ioc_args<std::string, std::reference_wrapper<UObject>>(args);
            std::string interface_name = std::any_cast<std::string>(args[0]);
            {matches_part}
            else
            {{
                throw IocException("No adapter with name" + interface_name + "was registered in IOC");
            }}
        }})
    }})->Execute();
}}