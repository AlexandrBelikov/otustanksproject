import os, glob, sys


class MethodParser:
    def __init__(self, line):
        line = line.replace("virtual", "").replace("= 0;", "").strip()
        self.m_arguments = line[ line.find("(") + 1 : line.find(")") ]
        self.m_argNames = MethodParser.GetArgNames(self.m_arguments)
        line = line.replace(self.m_arguments, "")

        for word in line.split(" "):
            if word.find("()") > -1:
                self.m_name = word
                break

        rest = line.split(self.m_name)
        self.m_name = self.m_name.replace("()", "")
        self.m_type = rest[0].strip()
        self.m_postfix = rest[1].strip()

    @staticmethod
    def GetArgNames(arguments):
        result = []
        arguments = arguments.split(", ")
        for argument in arguments:
            result.append(argument.split(" ")[-1])

        
        return ", ".join(result)


class InterfaceDescriptor:
    def __init__(self, lines, includes):
        self.m_publicMethods = []
        self.m_privateMethods = []
        self.m_public = False
        self.m_private = True
        self.m_includes = "".join(includes)

        for index in range(len(lines)):
            line = lines[index]

            if line.find("class") > -1:
                self.m_interfaceName = line.replace("class ", "").strip("\r\n")
                self.m_className = self.m_interfaceName.strip("I")
            elif line.find("public") > -1:
                self.m_public = True
                self.m_private = False
            elif line.find("private") > -1:
                self.m_private = True
                self.m_public = False
            # exclude destructor
            elif line.find("virtual") > -1 and line.find("= 0") > -1:
                if self.m_public:
                    self.m_publicMethods.append(MethodParser(line))
                else:
                    self.m_privateMethods.append(MethodParser(line))
            else:
                pass

    def SetSource(self, file_name):
        self.m_fileName = file_name.split("/")[-1]


class FunctionBlockMaker:
    m_getter_tpl = ""
    m_setter_tpl = ""

    def __init__(self, methods, className):
        if len(self.m_getter_tpl) == 0:
            self.ReadTemplates()

        block = ""
        for method in methods:
            if method.m_name.find("Get") > -1:
                block += self.m_getter_tpl.format(method = method, className = className)
            else:
                block += self.m_setter_tpl.format(method = method, className = className)

        self.m_result = block

    def Get(self):
        return self.m_result
        
    @staticmethod
    def ReadTemplates():
        getter_tpl_file = open("./templates/method_getter.tpl", "r")
        FunctionBlockMaker.m_getter_tpl = getter_tpl_file.read()
        getter_tpl_file.close()

        setter_tpl_file = open("./templates/method_setter.tpl", "r")
        FunctionBlockMaker.m_setter_tpl = setter_tpl_file.read()
        setter_tpl_file.close()


class AdapterFile:
    m_adapterTpl = ""
    m_dirName = "./generated"

    def __init__(self, interface_parsed):
        if len(AdapterFile.m_adapterTpl) == 0:
            tpl_file = open("./templates/adapter.tpl", "r")
            self.m_adapterTpl = tpl_file.read()
            tpl_file.close()

        if not os.path.exists(AdapterFile.m_dirName):
            os.mkdir(AdapterFile.m_dirName)

        interface_parsed.m_publicMethods = FunctionBlockMaker(
            interface_parsed.m_publicMethods, interface_parsed.m_className).Get()
        interface_parsed.m_privateMethods = FunctionBlockMaker(
            interface_parsed.m_privateMethods, interface_parsed.m_className).Get()

        self.m_classInfo = interface_parsed

    def Create(self):
        adapter_file_name = "{}/{}_adapter.h".format(
            self.m_dirName, self.m_classInfo.m_className.lower())
        adapter_file = open(adapter_file_name, "w")
            
        adapter_file.write(self.m_adapterTpl.format(desc = self.m_classInfo, 
            class_name_capitalized = self.m_classInfo.m_className.upper()))

        return adapter_file_name.split("/")[-1]


class HelperFile:
    m_helperTpl = ""
    m_helperList = ("// These dependencies must be registered to provide proper working of the app!\n" +
                    "// Please don't save anything here, the file may be overwritten!\n\n")
    
    def __init__(self):
        if len(HelperFile.m_helperTpl) == 0:
            HelperFile.TakeTemplates()

    def __del__(self):
        helper_file = open("./generated/dependencies_helper.h", "w")
        helper_file.write(HelperFile.m_helperList)
        helper_file.close()

    def Append(self, interface_parsed):
        for method in (interface_parsed.m_publicMethods + interface_parsed.m_privateMethods):
            HelperFile.m_helperList += HelperFile.m_helperTpl.format(
                interface_parsed.m_className, method.m_name)
        
    @staticmethod
    def TakeTemplates():
        tpl_file = open("./templates/dep_helper.tpl", "r")
        HelperFile.m_helperTpl = tpl_file.read()
        tpl_file.close()


class CommonIncluder:
    m_includerTpl = ""
    m_matchTpl = ""
    m_includePart = ""
    m_matchPart = ""

    def __init__(self):
        if len(CommonIncluder.m_includerTpl) == 0:
            CommonIncluder.TakeTemplates()

    def __del__(self):
        if len(CommonIncluder.m_matchPart) == 0:
            return

        CommonIncluder.m_matchPart = CommonIncluder.m_matchPart.strip("else \n")

        includer_file = open("./generated/generated_lib.h", "w")
        includer_file.write(CommonIncluder.m_includerTpl.format(
            includes = CommonIncluder.m_includePart, matches_part = CommonIncluder.m_matchPart))
        includer_file.close()

    def Append(self, filename, interface_parsed):
        CommonIncluder.m_matchPart += CommonIncluder.m_matchTpl.format(
            desc = interface_parsed)
        CommonIncluder.m_includePart += "#include \"" + filename + "\"\n"

    @staticmethod
    def TakeTemplates():
        includer_tpl_file = open("./templates/includer.tpl", "r")
        CommonIncluder.m_includerTpl = includer_tpl_file.read()
        includer_tpl_file.close()

        match_tpl_file = open("./templates/include_match.tpl", "r")
        CommonIncluder.m_matchTpl = match_tpl_file.read()
        match_tpl_file.close()


def find_markers(lines):
    found_markers = []

    for index in range(len(lines)):
        if lines[index].find("@@AUTOGEN_MARKER@@") > -1:
            found_markers.append(index)

    return found_markers


def find_classes(lines, markers):
    found_classes = []

    for index in range(len(lines)):
        if len(markers) == 0:
            return found_classes

        if (lines[index].find("};") > -1) and (index > markers[0]):
            found_classes.append(lines[markers[0] + 1 : index + 1])
            markers = markers[1:]


def find_includes(lines):
    found_includes = []

    for index in range(len(lines)):
        if lines[index].find("#include") > -1:
            found_includes.append(lines[index])

    return found_includes;