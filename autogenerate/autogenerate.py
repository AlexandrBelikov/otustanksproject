from tools import *

os.chdir(os.path.dirname(os.path.realpath(sys.argv[0])))
file_names = glob.glob('../**/*.h', recursive = True)
file_names = list(filter(lambda item: item.find("googletest") < 0, file_names))

helper_file = HelperFile()
common_includer = CommonIncluder()
created_files = []

for file_name in file_names:
    file = open(file_name, "r")
    lines = file.readlines()
    file.close()

    markers = find_markers(lines)
    class_parts = find_classes(lines, markers)

    if len(class_parts) > 0:
        include_parts = find_includes(lines)

    for part in class_parts:
        interface_parsed = InterfaceDescriptor(part, include_parts)
        interface_parsed.SetSource(file_name)
        helper_file.Append(interface_parsed)
        common_includer.Append(AdapterFile(interface_parsed).Create(), interface_parsed)

symlink = "../src/generated_lib"
if os.path.exists(symlink):
    os.remove(symlink)
os.symlink(os.getcwd() + "/generated", symlink)

print("---GENERATION PHASE DONE SUCCESSFULLY!---")