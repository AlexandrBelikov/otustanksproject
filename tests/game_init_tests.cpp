#include "gmock/gmock.h"
#include "uobject.h"
#include "vector.h"
#include "ioc.h"
#include "game_init_tests.h"

TEST_F(GameInitTest, init_procedure_test)
{
    std::string test_dep("TestDependency");
    std::string test_prop("TestProperty");
    UObject tank_for_proc_test({{test_prop, 0}});
    std::vector<std::string> test_commands = {test_dep, test_dep, test_dep};

    IoC::Resolve<p_lambda_t>(IOC::Register, {test_dep, scope_strategy_t(STRAT_ARGS_CAPTURE(&){
        validate_ioc_args<REF(UObject)>(args);
        return pLambdaCommand([&](){
            UObject& tank = UCast(REF(UObject), args[0]);

            tank[test_prop] = UCast(int, tank[test_prop]) + 1;
        });
    })})->Execute();

    // Тест на процедуру иницализации. Трижды выполняем тестовую зависимость на тестовом танке.
    init_tank(tank_for_proc_test, test_commands);
    EXPECT_EQ(UCast(int, tank_for_proc_test[test_prop]), 3);
}

TEST_F(GameInitTest, set_tank_user_test)
{
    // Стратегия "установить принадлежность танка игроку"
    register_user_for_tank_dep()->Execute();

    // Массив строк-имен команд на входе алгоритма
    std::vector<std::string> commands({
        set_user_for_tank_dep
    });

    UObject& params = get_game_params();
    // Инициализацию выполняем для всех танков
    for (tank_index = 0; tank_index < get_max_tanks(); tank_index++)
    {
        params[game_curr_tank_param] = tank_index;
        // Сначала создается пустой объект типа UObject
        UObject tank = IoC::Resolve<UObject>(new_tank_dep);
        init_tank(tank, commands);
        tanks_set.push_back(tank);
    }

    EXPECT_EQ(tanks_set.size(), get_max_tanks());
    const int half_of_tanks = get_max_tanks() / 2;

    // Тест на стратегию "установить принадлежность танка игроку"
    for (tank_index = 0; tank_index < half_of_tanks; ++tank_index)
    {
        UObject& tank = tanks_set[tank_index];
        EXPECT_EQ(user1_name, UCast(std::string, tank[tank_user_name]));
    }

    for (tank_index = half_of_tanks; tank_index < get_max_tanks(); ++tank_index)
    {
        UObject& tank = tanks_set[tank_index];
        EXPECT_EQ(user2_name, UCast(std::string, tank[tank_user_name]));
    }
}

TEST_F(GameInitTest, set_coordinate_test)
{
    // Стратегия "определить положение танка на игровом поле"
    register_set_coord_dep()->Execute();

    // Массив строк-имен команд на входе алгоритма
    std::vector<std::string> commands({
        set_tank_coord_dep
    });

    UObject& params = get_game_params();
    // Инициализацию выполняем для всех танков
    for (tank_index = 0; tank_index < get_max_tanks(); tank_index++)
    {
        params[game_curr_tank_param] = tank_index;
        // Сначала создается пустой объект типа UObject
        UObject tank = IoC::Resolve<UObject>(new_tank_dep);
        init_tank(tank, commands);
        tanks_set.push_back(tank);
    }

    EXPECT_EQ(tanks_set.size(), get_max_tanks());
    const int half_of_tanks = get_max_tanks() / 2;

    // Тест на стратегию "определить положение танка на игровом поле"
    for (tank_index = 0; tank_index < half_of_tanks; ++tank_index)
    {
        UObject& tank = tanks_set[tank_index];
        const vector_val_t x_pos = x_max * (tank_index % half_of_tanks) / half_of_tanks;
        const vector_val_t y_pos = y_max * 1 / 3;
        EXPECT_EQ( Vector({x_pos, y_pos}) , UCast(Vector, tank[UPROP_POSITION]));
    }

    for (tank_index = half_of_tanks; tank_index < get_max_tanks(); ++tank_index)
    {
        UObject& tank = tanks_set[tank_index];
        const vector_val_t x_pos = x_max * (tank_index % half_of_tanks) / half_of_tanks;
        const vector_val_t y_pos = y_max * 2 / 3;
        EXPECT_EQ( Vector({x_pos, y_pos}) , UCast(Vector, tank[UPROP_POSITION]));
    }
}

TEST_F(GameInitTest, set_direction_test)
{
    // Стратегия "установить направление танка"
    register_set_direction_dep()->Execute();

    // Массив строк-имен команд на входе алгоритма
    std::vector<std::string> commands({
        set_tank_direction
    });

    UObject& params = get_game_params();
    // Инициализацию выполняем для всех танков
    for (tank_index = 0; tank_index < get_max_tanks(); tank_index++)
    {
        params[game_curr_tank_param] = tank_index;
        // Сначала создается пустой объект типа UObject
        UObject tank = IoC::Resolve<UObject>(new_tank_dep);
        init_tank(tank, commands);
        tanks_set.push_back(tank);
    }

    EXPECT_EQ(tanks_set.size(), get_max_tanks());
    const int half_of_tanks = get_max_tanks() / 2;

    // Тест на стратегию "установить направление танка"
    for (tank_index = 0; tank_index < half_of_tanks; ++tank_index)
    {
        UObject& tank = tanks_set[tank_index];
        EXPECT_EQ(Vector({0, 1}), UCast(Vector, tank[UPROP_DIRECTION]));
    }

    for (tank_index = half_of_tanks; tank_index < get_max_tanks(); ++tank_index)
    {
        UObject& tank = tanks_set[tank_index];
        EXPECT_EQ(Vector({0, -1}), UCast(Vector, tank[UPROP_DIRECTION]));
    }
}