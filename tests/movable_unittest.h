#ifndef MOVABLE_UNITTEST_H
#define MOVABLE_UNITTEST_H

#include "gmock/gmock.h"
#include "movable_interface.h"

class MockIMovable : public IMovable
{
public:
    MOCK_METHOD(const Vector, GetPosition, (), (const, override));
    MOCK_METHOD(const Vector, GetVelocity, (), (const, override));
    MOCK_METHOD(void, SetPosition, (const Vector &another), (override));
};

#endif // MOVABLE_UNITTEST_H