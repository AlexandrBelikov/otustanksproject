#include "ioc_unittest.h"

Pool::Pool()
{}

void Pool::Push(PoolEntry *entry)
{
    Push(std::shared_ptr<PoolEntry>(entry));
}

void Pool::Push(p_entry_t entry)
{
    m_body.push_back(entry);
}

PoolGuard Pool::Take()
{
    if (m_body.size() > 0)
    {
        PoolGuard result(*this, m_body.back());
        m_body.pop_back();
        return result;
    }
    else
    {
        return PoolGuard(*this, nullptr);
    }
}

size_t Pool::Size()
{
    return m_body.size();
}

PoolGuard::PoolGuard(Pool& pool, p_entry_t entry) :
    m_pool(pool),
    m_entry(entry)
{}

PoolGuard::PoolGuard(const PoolGuard& another):
    m_pool(another.m_pool),
    m_entry(nullptr)
{
    m_entry.swap(another.m_entry);
}

PoolGuard::~PoolGuard()
{
    if(m_entry)
    {
        m_pool.Push(m_entry);
    }
}

PoolGuard::operator bool()
{
    return m_entry ? true : false;
}