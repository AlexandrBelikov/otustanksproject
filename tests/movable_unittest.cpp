#include "gmock/gmock.h"
#include "movable_unittest.h"
#include "move_command.h"

using ::testing::Return;
using ::testing::_;
using ::testing::Throw;

TEST(IMovable_test, succesfull_execution)
{
    MockIMovable movable;
    EXPECT_CALL(movable, GetPosition()).Times(1)
        .WillRepeatedly(Return(Vector({12, 5})));

    EXPECT_CALL(movable, GetVelocity()).Times(1)
        .WillRepeatedly(Return(Vector({-7, 3})));

    EXPECT_CALL(movable, SetPosition(Vector({5, 8}))).Times(1);

    MoveCommand cmd(movable);
    cmd.Execute();
}

TEST(IMovable_test, err_getting_position)
{
    MockIMovable movable;

    EXPECT_CALL(movable, GetPosition())
        .WillRepeatedly(Throw(std::bad_any_cast()));

    MoveCommand cmd(movable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}

TEST(IMovable_test, err_getting_velocity)
{
    MockIMovable movable;

    EXPECT_CALL(movable, GetVelocity())
        .WillRepeatedly(Throw(std::bad_any_cast()));

    MoveCommand cmd(movable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}

TEST(IMovable_test, err_setting_position)
{
    MockIMovable movable;

    EXPECT_CALL(movable, SetPosition(_))
        .WillRepeatedly(Throw(std::out_of_range("")));

    MoveCommand cmd(movable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}