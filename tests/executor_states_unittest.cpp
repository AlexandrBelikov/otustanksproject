#include <condition_variable>
#include <chrono>
#include <thread>
#include "gmock/gmock.h"

#include "thread_unittest.h"
#include "thread_queue.h"
#include "command_executor.h"
#include "rotate_command.h"
#include "move_command.h"
#include "uobject.h"
#include "movable_adapter.h"
#include "rotable_adapter.h"
#include "executor_lib.h"

using ::testing::Invoke;
using ::testing::Throw;

TEST(CommandExecutor_test, thread_started)
{
    std::shared_ptr<MockCommand> cmd = pMockCommand();
    std::condition_variable cv;

    EXPECT_CALL(*(cmd.get()), Execute)
        .Times(1)
        .WillRepeatedly([&cv](){cv.notify_all();});

    std::mutex stub_mutex;
    std::unique_lock stub_lock(stub_mutex);

    ThreadQueue queue;

    CommandExecutor executor(queue);
    queue.Push(cmd);
    
    cv.wait_for(stub_lock, std::chrono::seconds(10));
    executor.Dispose();
}

TEST(CommandExecutor_test, hard_stop)
{
    std::shared_ptr<MockCommand> cmd = pMockCommand();
    EXPECT_CALL(*(cmd.get()), Execute).Times(1); // Команда выполнится 1 раз
    
    ThreadQueue queue;
    queue.Push(cmd)
         .Push(pThreadHardStopCommand())
         .Push(cmd);
        
    CommandExecutor executor(queue);
    executor.TryJoin();
}

TEST(CommandExecutor_test, soft_stop)
{
    std::shared_ptr<MockCommand> cmd = pMockCommand();
    EXPECT_CALL(*(cmd.get()), Execute).Times(1) // Ожидаем вызова один раз
        .WillRepeatedly(Throw(ThreadSoftStopException("")));

    std::shared_ptr<MockCommand> test_cmd = pMockCommand();
    EXPECT_CALL(*(test_cmd.get()), Execute).Times(1); // Выполняется после soft stop

    UObject tank;
    RotableAdapter rotable(tank);
    MovableAdapter movable(tank);

    ThreadQueue queue;
    queue.Push(cmd)
         .Push(pRotateCommand(rotable))
         .Push(pMoveCommand(movable))
         .Push(test_cmd);
    CommandExecutor executor(queue);
    executor.TryJoin();
}

TEST(CommandExecutor_test, reset_regular_state)
{
    std::shared_ptr<MockCommand> cmd = pMockCommand();
    EXPECT_CALL(*(cmd.get()), Execute).Times(1) // Ожидаем вызова один раз
        .WillRepeatedly(Throw(ThreadSoftStopException("")));

    std::shared_ptr<MockCommand> test_cmd = pMockCommand();
    EXPECT_CALL(*(test_cmd.get()), Execute).Times(1); // Выполняется после soft stop

    std::shared_ptr<MockCommand> conditional_cmd = pMockCommand();
    std::condition_variable cv;
    EXPECT_CALL(*(conditional_cmd.get()), Execute)
        .Times(1)
        .WillRepeatedly([&cv](){cv.notify_all();});

    UObject tank;
    RotableAdapter rotable(tank);
    MovableAdapter movable(tank);

    ThreadQueue queue;
    queue.Push(cmd) // The executor is in soft stop state here
         .Push(pRotateCommand(rotable))
         .Push(pMoveCommand(movable))
         .Push(pThreadRegularStateCommand())
         .Push(conditional_cmd);
    CommandExecutor executor(queue);

    std::mutex stub_mutex;
    std::unique_lock stub_lock(stub_mutex);
    cv.wait_for(stub_lock, std::chrono::seconds(10));

    // The executor does nothing meanwhile
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    queue.Push(test_cmd)
         .Push(pThreadHardStopCommand());

    executor.TryJoin();
}

TEST(CommandExecutor_test, move_to_test)
{
    ThreadQueue queue_from;
    ThreadQueue queue_to;

    std::shared_ptr<MockCommand> test_cmd_0 = pMockCommand();
    EXPECT_CALL(*(test_cmd_0.get()), Execute).Times(0);

    std::shared_ptr<MockCommand> test_cmd_1 = pMockCommand();
    EXPECT_CALL(*(test_cmd_1.get()), Execute).Times(1);

    std::shared_ptr<MockCommand> conditional_cmd = pMockCommand();
    std::condition_variable cv;
    EXPECT_CALL(*(conditional_cmd.get()), Execute)
        .Times(1)
        .WillRepeatedly([&cv](){cv.notify_all();});

    queue_from.Push(pThreadMoveToCommand(queue_to))
              .Push(test_cmd_1)
              .Push(pThreadHardStopCommand())
              .Push(test_cmd_0)
              .Push(pThreadExitMoveToStateCommand())
              .Push(pThreadSoftStopCommand())
              .Push(conditional_cmd);

    EXPECT_EQ(queue_from.Size(), 7);
    EXPECT_EQ(queue_to.Size(), 0);

    CommandExecutor executor_from(queue_from);

    std::mutex stub_mutex;
    std::unique_lock stub_lock(stub_mutex);
    cv.wait_for(stub_lock, std::chrono::seconds(10));

    EXPECT_EQ(queue_from.Size(), 0);
    EXPECT_EQ(queue_to.Size(), 3);

    CommandExecutor executor_to(queue_to);

    executor_from.TryJoin();
    executor_to.TryJoin();
}