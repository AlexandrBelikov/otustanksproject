#include "burnable_unittest.h"
#include "burn_fuel_command.h"
#include "check_fuel_command.h"
#include "move_check_fuel_command.h"
#include "movable_unittest.h"

using ::testing::Return;
using ::testing::_;

TEST(IFuelBurnable_test, check_fuel_command_success)
{
    MockIFuelBurnable burnable;

    EXPECT_CALL(burnable, GetFuelConsumption()).Times(1)
        .WillRepeatedly(Return(1));

    // More fuel than needed
    EXPECT_CALL(burnable, GetFuel()).Times(1)
        .WillRepeatedly(Return(2));

    CheckFuelCommand cmd(burnable);
    EXPECT_NO_THROW(cmd.Execute());
}

TEST(IFuelBurnable_test, check_fuel_command_fail)
{
    MockIFuelBurnable burnable;

    EXPECT_CALL(burnable, GetFuelConsumption()).Times(1)
        .WillRepeatedly(Return(2));

    // Less fuel than needed
    EXPECT_CALL(burnable, GetFuel()).Times(1)
        .WillRepeatedly(Return(1));

    CheckFuelCommand cmd(burnable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}

TEST(IFuelBurnable_test, burn_fuel_command_success)
{
    MockIFuelBurnable burnable;

    EXPECT_CALL(burnable, GetFuelConsumption()).Times(1)
        .WillRepeatedly(Return(3));

    // More fuel than needed
    EXPECT_CALL(burnable, GetFuel()).Times(1)
        .WillRepeatedly(Return(20));

    EXPECT_CALL(burnable, SetFuel(17)).Times(1);

    BurnFuelCommand cmd(burnable);
    EXPECT_NO_THROW(cmd.Execute());
}

TEST(IFuelBurnable_test, burn_fuel_command_fail)
{
    MockIFuelBurnable burnable;

    EXPECT_CALL(burnable, GetFuelConsumption()).Times(1)
        .WillRepeatedly(Return(20));

    // Less fuel than needed
    EXPECT_CALL(burnable, GetFuel()).Times(1)
        .WillRepeatedly(Return(3));

    EXPECT_CALL(burnable, SetFuel(0)).Times(1);

    BurnFuelCommand cmd(burnable);
    EXPECT_NO_THROW(cmd.Execute());
}

TEST(IFuelBurnable_test, move_check_fuel_command_success)
{
    MockIFuelBurnable burnable;
    MockIMovable movable;

    EXPECT_CALL(burnable, GetFuelConsumption()).Times(2)
        .WillRepeatedly(Return(3));

    // More fuel than needed
    EXPECT_CALL(burnable, GetFuel()).Times(2)
        .WillRepeatedly(Return(20));

    EXPECT_CALL(burnable, SetFuel(17)).Times(1);

    EXPECT_CALL(movable, GetPosition()).Times(1)
        .WillRepeatedly(Return(Vector({3, 4})));
    
    EXPECT_CALL(movable, GetVelocity()).Times(1)
        .WillRepeatedly(Return(Vector({5, 6})));

    EXPECT_CALL(movable, SetPosition(Vector({8, 10}))).Times(1);

    MoveCheckFuelCommand cmd(movable, burnable);
    EXPECT_NO_THROW(cmd.Execute());
}

TEST(IFuelBurnable_test, move_check_fuel_command_fail)
{
    MockIFuelBurnable burnable;
    MockIMovable movable;

    EXPECT_CALL(burnable, GetFuelConsumption()).Times(1)
        .WillRepeatedly(Return(3));

    // Less fuel than needed
    EXPECT_CALL(burnable, GetFuel()).Times(1)
        .WillRepeatedly(Return(0));

    EXPECT_CALL(burnable, SetFuel(_)).Times(0);

    EXPECT_CALL(movable, GetPosition()).Times(0);
    
    EXPECT_CALL(movable, GetVelocity()).Times(0);

    EXPECT_CALL(movable, SetPosition(_)).Times(0);

    MoveCheckFuelCommand cmd(movable, burnable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}