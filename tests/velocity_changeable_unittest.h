#ifndef VELOCITY_CHANGEABLE_INTERFACE
#define VELOCITY_CHANGEABLE_INTERFACE

#include "gmock/gmock.h"
#include "velocity_changeable_interface.h"

class MockIVelocityChangeable : public IVelocityChangeable
{
public:
    MOCK_METHOD(int, GetInitialDirection, (), (const, override));
    MOCK_METHOD(int, GetDirection, (), (const, override));
    MOCK_METHOD(int, GetVelocity, (), (const, override));
    MOCK_METHOD(int, GetMaxDirections, (), (const, override));
    MOCK_METHOD(void, SetVelocityVector, (const Vector&), (override));

private:
    MOCK_METHOD(void, SetInitialDirection, (int), (override));
};

#endif // VELOCITY_CHANGEABLE_INTERFACE