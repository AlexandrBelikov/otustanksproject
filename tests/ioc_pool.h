#ifndef IOC_POOL_H
#define IOC_POOL_H

#include <vector>
#include <memory>

// Example of pool realization
class PoolEntry
{
public:
    PoolEntry(){};
};

typedef std::shared_ptr<PoolEntry> p_entry_t;
class PoolGuard;

class Pool
{
public:
    Pool();
    void Push(PoolEntry *entry);
    void Push(p_entry_t entry);
    PoolGuard Take();
    size_t Size();

private:
    std::vector<p_entry_t> m_body;
};

class PoolGuard
{
public:
    PoolGuard(Pool& pool, p_entry_t entry);
    PoolGuard(const PoolGuard& another);
    ~PoolGuard();
    operator bool();
    
private:
    Pool& m_pool;
    mutable p_entry_t m_entry;
};

#endif // IOC_POOL_H