#include "thread_unittest.h"
#include "macro_command.h"

using ::testing::Throw;

TEST(MacroCommand_test, execute_success)
{
    auto cmd1 = pMockCommand();
    auto cmd2 = pMockCommand();
    auto cmd3 = pMockCommand();

    EXPECT_CALL(*cmd1, Execute()).Times(1);
    EXPECT_CALL(*cmd2, Execute()).Times(1);
    EXPECT_CALL(*cmd3, Execute()).Times(1);

    auto macro = pMacroCommand(cmd1, cmd2, cmd3);
    EXPECT_NO_THROW(macro->Execute());
}

TEST(MacroCommand_test, execute_fail)
{
    auto cmd1 = pMockCommand();
    auto cmd2 = pMockCommand();
    auto cmd3 = pMockCommand();

    EXPECT_CALL(*cmd1, Execute()).Times(1);
    EXPECT_CALL(*cmd2, Execute()).Times(1)
        .WillOnce(Throw(CommandException()));
    EXPECT_CALL(*cmd3, Execute()).Times(0);

    auto macro = pMacroCommand(cmd1, cmd2, cmd3);
    EXPECT_THROW(macro->Execute(), CommandException);
}