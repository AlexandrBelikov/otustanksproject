#ifndef TESTABLE_INTERFACE_H
#define TESTABLE_INTERFACE_H

#include "vector.h"

//@@AUTOGEN_MARKER@@
class ITestable
{
public:
    virtual const Vector GetTestPoint() const = 0;
    virtual void SetTestPoint(const Vector& vector) = 0;
    virtual void AskQuestion() = 0;
    virtual ~ITestable(){};
};

#endif // TESTABLE_INTERFACE_H