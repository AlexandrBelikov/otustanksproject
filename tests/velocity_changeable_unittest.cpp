#include "gmock/gmock-matchers.h"
#include "command.h"
#include "change_velocity_command.h"
#include "velocity_changeable_unittest.h"
#include "command_exception.h"
#include "rotable_unittest.h"
#include "burnable_unittest.h"
#include "rotate_change_velocity.h"

#define DOUBLE_PRECISION 0.0001

using ::testing::Return;
using ::testing::Throw;

// For double comparison with defined precision
MATCHER_P(IsVectorEqual, another_vector, "")
{
    for (int i = 0; i < VECTOR_SIZE; i++)
    {
        if (std::fabs(another_vector.GetBody(i) - arg.GetBody(i)) > DOUBLE_PRECISION)
        {
            return false;
        }
    }
    return true;
}

TEST(VelocityChangeable_test, successfull_execution_1)
{
    MockIVelocityChangeable velChangeable;

    EXPECT_CALL(velChangeable, GetInitialDirection()).Times(1)
        .WillRepeatedly(Return(0));

    EXPECT_CALL(velChangeable, GetDirection()).Times(1)
        .WillRepeatedly(Return(2));

    EXPECT_CALL(velChangeable, GetVelocity()).Times(1)
        .WillRepeatedly(Return(5));

    EXPECT_CALL(velChangeable, GetMaxDirections()).Times(1)
        .WillRepeatedly(Return(8));

    EXPECT_CALL(velChangeable, SetVelocityVector(IsVectorEqual(Vector({0, 5}))))
        .Times(1);

    p_cmd_t cmd = pChangeVelocityCommand(velChangeable);
    EXPECT_NO_THROW(cmd->Execute());
}

TEST(VelocityChangeable_test, successfull_execution_2)
{
    MockIVelocityChangeable velChangeable;

    EXPECT_CALL(velChangeable, GetInitialDirection()).Times(1)
        .WillRepeatedly(Return(0));

    EXPECT_CALL(velChangeable, GetDirection()).Times(1)
        .WillRepeatedly(Return(12));

    EXPECT_CALL(velChangeable, GetVelocity()).Times(1)
        .WillRepeatedly(Return(7));

    EXPECT_CALL(velChangeable, GetMaxDirections()).Times(1)
        .WillRepeatedly(Return(8));

    EXPECT_CALL(velChangeable, SetVelocityVector(IsVectorEqual(Vector({-7, 0}))))
        .Times(1);

    p_cmd_t cmd = pChangeVelocityCommand(velChangeable);
    EXPECT_NO_THROW(cmd->Execute());
}

TEST(VelocityChangeable_test, failing_execution)
{
    MockIVelocityChangeable velChangeable;

    EXPECT_CALL(velChangeable, GetInitialDirection()).Times(1)
        .WillRepeatedly(Throw(std::bad_any_cast()));

    EXPECT_CALL(velChangeable, GetDirection()).Times(0);
    EXPECT_CALL(velChangeable, GetVelocity()).Times(0);
    EXPECT_CALL(velChangeable, GetMaxDirections()).Times(0);
    EXPECT_CALL(velChangeable, SetVelocityVector).Times(0);

    p_cmd_t cmd = pChangeVelocityCommand(velChangeable);
    EXPECT_THROW(cmd->Execute(), CommandException);
}

TEST(VelocityChangeable_test, rotate_change_velocity_command_success)
{
    MockIVelocityChangeable velChangeable;
    MockIRotable rotable;

    EXPECT_CALL(velChangeable, GetMaxDirections).Times(1)
        .WillRepeatedly(Return(4));

    EXPECT_CALL(rotable, GetMaxDirections).Times(1)
        .WillRepeatedly(Return(4));

    p_cmd_t cmd = pRotateChangeVelocityCommand(velChangeable, rotable);
    EXPECT_NO_THROW(cmd->Execute());
}

TEST(VelocityChangeable_test, rotate_change_velocity_command_fail)
{
    MockIVelocityChangeable velChangeable;
    MockIRotable rotable;

    EXPECT_CALL(rotable, GetMaxDirections).Times(1)
        .WillRepeatedly(Throw(std::bad_any_cast()));

    EXPECT_CALL(velChangeable, GetMaxDirections).Times(0);

    p_cmd_t cmd = pRotateChangeVelocityCommand(velChangeable, rotable);
    EXPECT_THROW(cmd->Execute(), CommandException);
}