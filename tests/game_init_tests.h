#ifndef GAME_INIT_TEST_H
#define GAME_INIT_TEST_H

#include "game_init_lib.h"

using namespace GameInitCommons;
using namespace GameInitStrategies;

class GameInitTest : public ::testing::Test
{
protected:
    vector_val_t x_max = 1000;
    vector_val_t y_max = 900;
    std::vector<UObject> tanks_set;
    int tank_index = 0;

    void SetUp() override
    {
        UObject game_params({
            { game_curr_tank_param, 0 },
            { game_max_tanks_param, 8}, // Get this params from setup
            { game_field_size_param, Vector({x_max, y_max}) }, //
        });

        scope_id_t new_scope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
        IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {new_scope})->Execute();

        register_game_parameters(game_params)->Execute();
        register_new_tank_dep()->Execute();
    }
};

#endif // GAME_INIT_TEST_H