#include "rotable_unittest.h"
#include "rotate_command.h"

using ::testing::Return;
using ::testing::_;
using ::testing::Throw;

TEST(IRotable_test, succesfull_execution) 
{
    MockIRotable rotable;

    EXPECT_CALL(rotable, GetDirection()).Times(1)
        .WillRepeatedly(Return(0));

    EXPECT_CALL(rotable, GetAngularVelocity()).Times(1)
        .WillRepeatedly(Return(6));

    EXPECT_CALL(rotable, GetMaxDirections()).Times(1)
        .WillRepeatedly(Return(4));

    EXPECT_CALL(rotable, SetDirection(2)).Times(1);

    RotateCommand cmd(rotable);
    cmd.Execute();
}

TEST(IRotable_test, err_getting_direction)
{
    MockIRotable rotable;

    EXPECT_CALL(rotable, GetDirection())
        .WillRepeatedly(Throw(std::bad_any_cast()));

    RotateCommand cmd(rotable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}

TEST(IRotable_test, err_getting_velocity)
{
    MockIRotable rotable;

    EXPECT_CALL(rotable, GetAngularVelocity())
        .WillRepeatedly(Throw(std::bad_any_cast()));

    RotateCommand cmd(rotable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}

TEST(IRotable_test, err_getting_max_directions)
{
    MockIRotable rotable;

    EXPECT_CALL(rotable, GetMaxDirections())
        .WillRepeatedly(Throw(std::bad_any_cast()));

    RotateCommand cmd(rotable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}

TEST(IRotable_test, err_setting_direction)
{
    MockIRotable rotable;

    EXPECT_CALL(rotable, SetDirection(_))
        .WillRepeatedly(Throw(std::out_of_range("")));

    // Suppressing div by 0 exception
    EXPECT_CALL(rotable, GetMaxDirections())
        .WillRepeatedly(Return(1));

    RotateCommand cmd(rotable);
    EXPECT_THROW(cmd.Execute(), CommandException);
}
