#ifndef BURNABLE_UNITTEST_H
#define BURNABLE_UNITTEST_H

#include "gmock/gmock.h"
#include "fuel_burnable_interface.h"

class MockIFuelBurnable : public IFuelBurnable
{
public:
    MOCK_METHOD(int, GetFuelConsumption, (), (const, override));
    MOCK_METHOD(int, GetFuel, (), (const, override));
    MOCK_METHOD(void, SetFuel, (int), (override));
};

#endif // BURNABLE_UNITTEST_H