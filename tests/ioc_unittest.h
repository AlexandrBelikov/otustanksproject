#ifndef IOC_UNITTEST_H
#define IOC_UNITTEST_H

#include <string>
#include "ioc_pool.h"

class TestClass
{
public:
    TestClass(int int_val, std::string str_val) :
        m_int(int_val),
        m_string(str_val)
    {}

    bool operator ==(const TestClass& another) const
    {
        return (m_int == another.m_int) && (m_string == another.m_string);
    }

private:
    int m_int;
    std::string m_string;
};

#endif // IOC_UNITTEST_H