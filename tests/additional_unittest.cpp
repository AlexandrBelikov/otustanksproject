#include "rotable_unittest.h"
#include "gtest/gtest.h"
#include "uobject.h"
#include "rotable_adapter.h"

TEST(My_RotableAdapter_test, correct_returned_vals)
{
    UObject obj({{RotableAdapter::m_angularVelocityName, "TooMuch"}, {RotableAdapter::m_directionName, 1}});
    RotableAdapter rotable(obj);

    EXPECT_THROW(rotable.GetAngularVelocity(), std::bad_any_cast);
    EXPECT_THROW(rotable.GetMaxDirections(), std::out_of_range);
    EXPECT_EQ (rotable.GetDirection(), 1);
}