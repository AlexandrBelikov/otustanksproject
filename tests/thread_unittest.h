#ifndef THREAD_UNITTEST_H
#define THREAD_UNITTEST_H

#include <memory>
#include "gmock/gmock.h"
#include "command.h"

#define pMockCommand std::make_shared<MockCommand>

class MockCommand : public Command
{
public:
    MOCK_METHOD(void, Execute, (), (override));

private:
    MOCK_METHOD(void, CommandToExecute, (), (override));
};

#endif // THREAD_UNITTEST_H