#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "ioc.h"
#include "uobject.h"
#include "generated_lib.h"
#include "rotable_unittest.h"

TEST(Autogen_test, adapters_registration)
{
    UObject tank;

    EXPECT_THROW(IoC::Resolve<Testable>(IOC::Adapter, {std::string("ITestable"), std::ref(tank)}), IocException);
    register_generated_deps();
    EXPECT_NO_THROW(IoC::Resolve<Testable>(IOC::Adapter, {std::string("ITestable"), std::ref(tank)}));
}

TEST(Autogen_test, dependencies_registration)
{
    EXPECT_THROW(IoC::Resolve<const Vector>("Testable.GetTestPoint"), IocException);
    
    const std::string test_point("TEST_POINT");
    UObject tank({
        {test_point, Vector({-3, 144})}
    });

    MockIRotable rotable;
    EXPECT_CALL(rotable, GetDirection()).Times(1);

    // Strategies copied from dependencies helper
    IoC::Resolve<p_lambda_t>(IOC::Register, { std::string("Testable.GetTestPoint"),
        scope_strategy_t([&test_point](const ioc_args_t& args)->std::any
        {
            validate_ioc_args<std::reference_wrapper<UObject>>(args);
            UObject& obj = UCast(std::reference_wrapper<UObject>, args[0]);
            return obj[test_point];
        })
    })->Execute();

    IoC::Resolve<p_lambda_t>(IOC::Register, { std::string("Testable.SetTestPoint"),
        scope_strategy_t([&test_point](const ioc_args_t& args)->std::any
        {
            return pLambdaCommand([&test_point, args]()
            {
                validate_ioc_args<std::reference_wrapper<UObject>, Vector>(args);
                UObject& obj = UCast(std::reference_wrapper<UObject>, args[0]);
                obj[test_point] = args[1];
            });
        })
    })->Execute();

    IoC::Resolve<p_lambda_t>(IOC::Register, { std::string("Testable.AskQuestion"),
        scope_strategy_t([&rotable](const ioc_args_t& args)->std::any
        {
            return pLambdaCommand([args, &rotable]()
            {
                // impact rotable here to pass the test
                rotable.GetDirection();
                std::cout << "42!" << std::endl;
            });
        })
    })->Execute();

    // Tests part
    auto testable = IoC::Resolve<Testable>(IOC::Adapter, {std::string("ITestable"), std::ref(tank)});
    EXPECT_EQ(testable.GetTestPoint(), Vector({-3, 144}));
    EXPECT_NO_THROW(testable.SetTestPoint(Vector({57, -33})));
    EXPECT_EQ(testable.GetTestPoint(), Vector({57, -33}));
    testable.AskQuestion();
}