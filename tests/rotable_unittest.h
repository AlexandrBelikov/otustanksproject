#ifndef ROTABLE_UNITTEST_H
#define ROTABLE_UNITTEST_H

#include "gmock/gmock.h"
#include "rotable_interface.h"

class MockIRotable : public IRotable
{
public:
    MOCK_METHOD(int, GetDirection, (), (const, override));
    MOCK_METHOD(void, SetDirection, (int newDir), (override));
    MOCK_METHOD(int, GetAngularVelocity, (), (const, override));
    MOCK_METHOD(int, GetMaxDirections, (), (const, override));
};

#endif // ROTABLE_UNITTEST_H