#include <any>

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "ioc.h"
#include "command.h"
#include "movable_adapter.h"
#include "uobject.h"
#include "ioc_unittest.h"
#include "thread_queue.h"
#include "command_executor.h"

TEST(IOC_test, arguments_types_validation_success)
{
    ioc_args_t args({1, 2.3, std::string("Test"), true, pLambdaCommand([](){})});
    EXPECT_NO_THROW((validate_ioc_args<int, double, std::string, bool, p_lambda_t>(args)));

    EXPECT_THROW((validate_ioc_args<double, int>(args)), IocException);
    try
    {
        validate_ioc_args<double, int>(args);
    }
    catch(const IocException& e)
    {
        std::cout << e.what() << std::endl;
    }
    
    EXPECT_THROW((validate_ioc_args<int, int, std::string, bool, bool, double>(args)), IocException);
    try
    {
        validate_ioc_args<int, int, std::string, bool, double, scope_strategy_t>(args);
    }
    catch(const IocException& e)
    {
        std::cout << e.what() << std::endl;
    }
}

TEST(IOC_test, return_new_copy)
{
    scope_id_t new_scope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {new_scope})->Execute();

    std::string dep_movable("DepMovable");
    UObject tank({
        {UPROP_POSITION, Vector({-7, 13})}
    });

    IoC::Resolve<p_lambda_t>(IOC::Register, { dep_movable, 
        scope_strategy_t([&tank](const ioc_args_t &)->std::any
        {
            return MovableAdapter(tank);
        }) 
    })->Execute();

    MovableAdapter adapter = IoC::Resolve<MovableAdapter>(dep_movable);
    EXPECT_EQ(adapter.GetPosition(), Vector({-7, 13}));
}

TEST(IOC_test, return_ref)
{
    scope_id_t new_scope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {new_scope})->Execute();

    std::string ref_tank("RefTank");
    IoC::Resolve<p_lambda_t>(IOC::Register, { ref_tank,
        scope_strategy_t([ref_tank](const ioc_args_t &)->std::any
        {
            static UObject tank({
                {UPROP_POSITION, Vector({0, 0})}
            });
            return std::ref(tank);
        })
    })->Execute();

    UObject& ref_tank1 = IoC::Resolve<std::reference_wrapper<UObject>>(ref_tank);
    MovableAdapter movable1(ref_tank1);
    EXPECT_NO_THROW(movable1.SetPosition(Vector({2, -5})));

    UObject& ref_tank2 = IoC::Resolve<std::reference_wrapper<UObject>>(ref_tank);
    MovableAdapter movable2(ref_tank2);
    EXPECT_EQ(movable2.GetPosition(), Vector({2, -5}));
}

TEST(IOC_test, return_pool_entry)
{
    scope_id_t new_scope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {new_scope})->Execute();

    Pool pool;
    pool.Push(new PoolEntry());
    pool.Push(new PoolEntry());

    std::string dep_pool("DepPool");
    IoC::Resolve<p_lambda_t>(IOC::Register, { dep_pool, 
        scope_strategy_t([&pool](const ioc_args_t &)->std::any
        {
            return pool.Take();
        })
    })->Execute();

    EXPECT_EQ(pool.Size(), 2);
    {
        PoolGuard guard(IoC::Resolve<PoolGuard>(dep_pool));
        EXPECT_EQ(guard ? 1 : 0, 1);
        EXPECT_EQ(pool.Size(), 1);
        // Frees via destructor
    }
    EXPECT_EQ(pool.Size(), 2);
}

TEST(IOC_test, validate_types_in_strategy)
{
    scope_id_t new_scope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {new_scope})->Execute();

    std::string test_dep("DepDumb");
    IoC::Resolve<p_lambda_t>(IOC::Register, { test_dep,
        scope_strategy_t([](const ioc_args_t& args)->std::any
        {
            validate_ioc_args<int, std::string>(args);
            return TestClass(std::any_cast<int>(args[0]), 
                std::any_cast<std::string>(args[1]));
        })
    })->Execute();

    EXPECT_NO_THROW(IoC::Resolve<TestClass>(test_dep, { 1, std::string("a") }));
    EXPECT_THROW(IoC::Resolve<TestClass>(test_dep, { 1.75 }), IocException);
}

TEST(IOC_test, dependency_injection)
{
    scope_id_t new_scope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {new_scope})->Execute();

    std::string test_dep("DepDumb");
    std::string test_first_arg("DepTestFirstArg");
    std::string test_second_arg("DepTestSecondArg");

    IoC::Resolve<p_lambda_t>(IOC::Register, { test_first_arg,
        scope_strategy_t([](const ioc_args_t&)->std::any
        {
            return 8;
        })
    })->Execute();

    IoC::Resolve<p_lambda_t>(IOC::Register, { test_second_arg,
        scope_strategy_t([](const ioc_args_t&)->std::any
        {
            return std::string("Hello!");
        })
    })->Execute();

    IoC::Resolve<p_lambda_t>(IOC::Register, { test_dep,
        scope_strategy_t([&](const ioc_args_t& args)->std::any
        {
            return TestClass(IoC::Resolve<int>(test_first_arg),
                IoC::Resolve<std::string>(test_second_arg));
        })
    })->Execute();

    EXPECT_NO_THROW(IoC::Resolve<TestClass>(test_dep));
    EXPECT_EQ(IoC::Resolve<TestClass>(test_dep), TestClass(8, "Hello!"));
}

TEST(IOC_test, multithread_test)
{
    scope_id_t new_scope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {new_scope})->Execute();

    std::string test_dep("DepTest");
    std::function<void(int)> test_dep_func = [&test_dep](int val){
        IoC::Resolve<p_lambda_t>(IOC::Register, { test_dep, 
            scope_strategy_t([val](const ioc_args_t& args)->std::any
            {
                return val;
            })
        })->Execute();
    };

    std::function<void(int)> check_dep_func = [&test_dep](int val){
        EXPECT_EQ(IoC::Resolve<int>(test_dep), val);
    };

    p_cmd_t check_not_equal_func = pLambdaCommand([&test_dep](){
        EXPECT_NE(IoC::Resolve<int>(test_dep), 9);
    });

    p_cmd_t test_command_1 = pLambdaCommand([&test_dep_func](){test_dep_func(1);});
    p_cmd_t test_command_2 = pLambdaCommand([&test_dep_func](){test_dep_func(2);});

    p_cmd_t check_command_1 = pLambdaCommand([&check_dep_func](){check_dep_func(1);});
    p_cmd_t check_command_2 = pLambdaCommand([&check_dep_func](){check_dep_func(2);});

    ThreadQueue queue1;
    queue1.Push(test_command_1).Push(check_command_1)
        .Push(pThreadSoftStopCommand()).Push(check_not_equal_func);

    ThreadQueue queue2;
    queue2.Push(test_command_2).Push(check_command_2)
        .Push(pThreadSoftStopCommand()).Push(check_not_equal_func);

    CommandExecutor thread1(queue1);
    CommandExecutor thread2(queue2);

    test_dep_func(0);
    check_dep_func(0);
    check_not_equal_func->Execute();

    thread1.TryJoin();
    thread2.TryJoin();
}

TEST(IOC_test, multiscope_test)
{
    scope_id_t new_scope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {new_scope})->Execute();

    std::string test_dep("DepTest");
    std::function<void(int)> test_dep_func = [&test_dep](int val){
        IoC::Resolve<p_lambda_t>(IOC::Register, { test_dep, 
            scope_strategy_t([val](const ioc_args_t& args)->std::any
            {
                return val;
            })
        })->Execute();
    };

    std::function<void(int)> check_dep_func = [&test_dep](int val){
        EXPECT_EQ(IoC::Resolve<int>(test_dep), val);
    };

    p_cmd_t check_not_equal_func = pLambdaCommand([&test_dep](){
        EXPECT_NE(IoC::Resolve<int>(test_dep), 9);
    });

    scope_id_t scope_user_1 = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    p_cmd_t set_user_scope_1 = IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {scope_user_1});

    scope_id_t scope_user_2 = IoC::Resolve<scope_id_t>(IOC::Scope::New);
    p_cmd_t set_user_scope_2 = IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {scope_user_2});

    set_user_scope_1->Execute();
    test_dep_func(1);

    set_user_scope_2->Execute();
    test_dep_func(2);

    set_user_scope_1->Execute();
    check_dep_func(1);

    set_user_scope_2->Execute();
    check_dep_func(2);

    set_user_scope_1->Execute();
    check_not_equal_func->Execute();

    set_user_scope_2->Execute();
    check_not_equal_func->Execute();
}
