#ifndef UOBJECT_PROPERTIES_H
#define UOBJECT_PROPERTIES_H

// Centralized definitions of uobject properties
#define UPROP_POSITION          "Position"
#define UPROP_VELOCITY          "Velocity"
#define UPROP_DIRECTION         "Direction"
#define UPROP_FUEL_AMOUNT       "FuelAmount"
#define UPROP_MAX_DIRECTIONS    "MaxDirections"
#define UPROP_VELOCITY_VECTOR   "VelocityVector"
#define UPROP_ANGULAR_VELOCITY  "AngularVelocity"
#define UPROP_FUEL_CONSUMPTION  "FuelConsumption"
#define UPROP_INITIAL_DIRECTION "InitialDirection"

#endif // UOBJECT_PROPERTIES_H