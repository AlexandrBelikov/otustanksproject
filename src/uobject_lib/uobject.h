#ifndef UOBJECT_H
#define UOBJECT_H

#include <any>
#include <map>
#include <string>
#include "uobject_properties.h"

#define UCast(type, what) std::any_cast<type>(what)
typedef std::map<std::string, std::any> uobj_body_t;

class UObject
{
public:
    UObject()
    {}

    UObject(const uobj_body_t& body) :
        m_body(body)
    {}

    UObject(const UObject& another) :
        m_body(another.m_body)
    {}

    std::any& operator[] (const std::string& key)
    {
        return m_body.at(key);
    }

    bool Insert(const std::string& key, const std::any& value)
    {
        return m_body.insert_or_assign(key, value).second;
    }

    const uobj_body_t& GetBody() const
    {
        return m_body;
    }

private:
    uobj_body_t m_body;
};

#endif // UOBJECT_H