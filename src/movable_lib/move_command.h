#ifndef MOVE_COMMAND_H
#define MOVE_COMMAND_H

#include "command.h"
#include "movable_interface.h"

#define pMoveCommand std::make_shared<MoveCommand>

class MoveCommand : public Command
{
public:
    MoveCommand(IMovable &movable) :
        m_movable(movable)
    {}

private:
    IMovable &m_movable;

    void CommandToExecute()
    {
        m_movable.SetPosition(m_movable.GetPosition() + 
            m_movable.GetVelocity());
    }
};

#endif // MOVE_COMMAND_H