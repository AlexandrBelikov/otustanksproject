#include "movable_adapter.h"

const std::string MovableAdapter::m_positionName = UPROP_POSITION;
const std::string MovableAdapter::m_velocityVectorName = UPROP_VELOCITY_VECTOR;

const Vector MovableAdapter::GetPosition() const
{
    return UCast(Vector, m_obj[m_positionName]);
}

const Vector MovableAdapter::GetVelocity() const
{
    return UCast(Vector, m_obj[m_velocityVectorName]);
}

void MovableAdapter::SetPosition(const Vector& another)
{
    m_obj[m_positionName] = another;
}