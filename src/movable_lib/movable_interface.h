#ifndef MOVABLE_INTERFACE_H
#define MOVABLE_INTERFACE_H

#include "vector.h"

class IMovable
{
public:
    virtual const Vector GetPosition() const = 0;
    virtual const Vector GetVelocity() const = 0;
    virtual void SetPosition(const Vector& another) = 0;
    virtual ~IMovable(){};
};

#endif // MOVABLE_INTERFACE_H