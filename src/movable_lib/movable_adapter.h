#ifndef MOVABLE_ADAPTER_H
#define MOVABLE_ADAPTER_H

#include "vector.h"
#include "uobject.h"
#include "movable_interface.h"

class MovableAdapter : public IMovable
{
public:
    static const std::string m_positionName;
    static const std::string m_velocityVectorName;

    MovableAdapter(UObject& obj) :
        m_obj(obj)
    {}

    const Vector GetPosition() const;
    const Vector GetVelocity() const;
    void SetPosition(const Vector& another);

private:
    UObject& m_obj;
};

#endif // MOVABLE_ADAPTER_H