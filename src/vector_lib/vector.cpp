#include "vector.h"

/**
 * @brief Vector class members part
 */

Vector Vector::operator+=(const Vector& another)
{
    return (*this + another);
}

const vector_body_t& Vector::GetBody() const
{
    return m_body;
}

vector_val_t Vector::GetBody(int i) const
{
    return m_body.at(i);
}

/**
 * @brief Vector overloaded operators part
 */

Vector operator+ (const Vector& v1, const Vector& v2)
{
    vector_body_t result(v1.GetBody());
    for (int i = 0; i < VECTOR_SIZE; ++i)
    {
        result[i] += v2.GetBody(i);
    }
    return Vector(std::move(result));
}

bool operator== (const Vector& v1, const Vector& v2)
{
    return v1.GetBody() == v2.GetBody();
}