#ifndef VECTOR_H
#define VECTOR_H

#include <array>
#include <utility>

#define VECTOR_SIZE 2
#define AS_VECTOR_BODY_TYPE as_double
typedef double vector_val_t;
typedef std::array<vector_val_t, VECTOR_SIZE> vector_body_t;

class Vector;
Vector operator+ (const Vector& v1, const Vector& v2);
bool operator==  (const Vector& v1, const Vector& v2);

class Vector
{
public:
    enum Dimensions
    {
        DIM_X = 0,
        DIM_Y = 1,
        DIM_Z = 2
    };

    Vector(const vector_body_t&& body = { 0 }) :
        m_body(body)
    {}

    Vector(const Vector& another) :
        m_body(another.GetBody())
    {}

    Vector operator+=(const Vector& another);
    const vector_body_t& GetBody() const;
    vector_val_t GetBody(int i) const;

private:
    vector_body_t m_body;
};

#endif // VECTOR_H