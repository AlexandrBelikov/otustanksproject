#ifndef THREAD_SOFT_STOP_H
#define THREAD_SOFT_STOP_H

#include "executor_state.h"
#include "state_exception.h"
#include "command.h"

class SoftStopState : public State
{
public:
    SoftStopState(ThreadQueue& queue) :
        State(queue)
    {}

private:
    p_state_t Strategy() override
    {
        if (State::m_queue.Size() > 0)
        {
            State::m_queue.Take()->Execute();
        }
        else
        {
            return p_state_t(nullptr);
        }
        return shared_from_this();
    }
};

class ThreadSoftStopException : public StateException
{
public:
    ThreadSoftStopException(const std::string& what) :
        StateException(what)
    {}

    p_state_t GetState(ThreadQueue& queue) const override
    {
        return std::make_shared<SoftStopState>(queue);
    }
};

#define pThreadSoftStopCommand std::make_shared<ThreadSoftStopCommand>

class ThreadSoftStopCommand : public Command
{
public:
    ThreadSoftStopCommand()
    {}

    void Execute() override
    {
        throw ThreadSoftStopException("Soft stop state exception was thrown");
    }

private:
    void CommandToExecute()
    {}
};

#endif // THREAD_SOFT_STOP_H