#ifndef STATE_EXCEPTION_H
#define STATE_EXCEPTION_H

#include <exception>
#include <string>
#include "executor_state.h"
#include "thread_queue.h"

class StateException : public std::exception
{
public:
    StateException(const std::string& error = "") :
        m_error(error)
    {}

    const char* what() const noexcept
    {
        return m_error.c_str();
    }

    virtual p_state_t GetState(ThreadQueue& queue) const = 0;

private:
    std::string m_error;
};

#endif // STATE_EXCEPTION_H