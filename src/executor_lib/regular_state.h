#ifndef REGULAR_STATE_H
#define REGULAR_STATE_H

#include "executor_state.h"
#include "state_exception.h"
#include "command.h"

class RegularState : public State
{
public:
    RegularState(ThreadQueue& queue) :
        State(queue)
    {}

private:
    p_state_t Strategy() override
    {
        p_cmd_t command = State::m_queue.Take();
        if (command)
        {
            command->Execute();
        }
        else
        {
            State::m_queue.Wait();
        }
        return shared_from_this();
    }
};

class RegularStateException : public StateException
{
public:
    RegularStateException(const std::string& what) :
        StateException(what)
    {}

    p_state_t GetState(ThreadQueue& queue) const override
    {
        return std::make_shared<RegularState>(queue);
    }
};

#define pThreadRegularStateCommand std::make_shared<ThreadRegularStateCommand>

class ThreadRegularStateCommand : public Command
{
public:
    ThreadRegularStateCommand()
    {}

    void Execute() override
    {
        throw RegularStateException("Regular state exception was thrown");
    }

private:
    void CommandToExecute()
    {}
};

#endif // REGULAR_STATE_H