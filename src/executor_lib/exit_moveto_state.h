#ifndef EXIT_MOVETO_STATE_H
#define EXIT_MOVETO_STATE_H

#include "regular_state.h"

#define pThreadExitMoveToStateCommand std::make_shared<ThreadExitMoveToStateCommand>

class ThreadExitMoveToStateCommand : public Command
{
public:
    ThreadExitMoveToStateCommand()
    {}

    void Execute() override
    {
        throw RegularStateException("Regular state exception was thrown");
    }

private:
    void CommandToExecute()
    {}
};

#endif // EXIT_MOVETO_STATE_H