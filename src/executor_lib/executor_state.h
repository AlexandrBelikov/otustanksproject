#ifndef EXECUTOR_STATE_H
#define EXECUTOR_STATE_H

#include <memory>
#include "thread_queue.h"

class State;
typedef std::shared_ptr<State> p_state_t;

class State : public std::enable_shared_from_this<State>
{
public:
    State(ThreadQueue& queue);
    virtual ~State();

    p_state_t Next();

protected:
    ThreadQueue& m_queue;

private:
    virtual p_state_t Strategy() = 0;
};

#endif // EXECUTOR_STATE_H