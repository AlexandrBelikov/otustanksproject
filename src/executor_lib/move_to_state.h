#ifndef MOVE_TO_STATE_H
#define MOVE_TO_STATE_H

#include "executor_state.h"
#include "state_exception.h"
#include "command.h"
#include "regular_state.h"
#include "exit_moveto_state.h"

class MoveToState : public State
{
public:
    MoveToState(ThreadQueue& queue, ThreadQueue& moveToQueue) :
        State(queue),
        m_moveToQueue(moveToQueue)
    {}

private:
    ThreadQueue& m_moveToQueue;

    p_state_t Strategy() override
    {
        p_cmd_t command = State::m_queue.Take();
        if (command)
        {
            if (dynamic_cast<ThreadExitMoveToStateCommand*>(command.get()))
            {
                command->Execute();
            }
            else
            {
                m_moveToQueue.Push(command);
            }
        }
        else
        {
            State::m_queue.Wait();
        }
        return shared_from_this();
    }
};

class MoveToStateException : public StateException
{
public:
    MoveToStateException(const std::string& what, ThreadQueue& queue) :
        StateException(what),
        m_moveToQueue(queue)
    {}

    p_state_t GetState(ThreadQueue& queue) const override
    {
        return std::make_shared<MoveToState>(queue, m_moveToQueue);
    }

private:
    ThreadQueue& m_moveToQueue;
};

#define pThreadMoveToCommand std::make_shared<ThreadMoveToCommand>

class ThreadMoveToCommand : public Command
{
public:
    ThreadMoveToCommand(ThreadQueue& queue) :
        m_moveToQueue(queue)
    {}

    void Execute() override
    {
        throw MoveToStateException("MoveTo state exception was thrown", m_moveToQueue);
    }

private:
    ThreadQueue& m_moveToQueue;

    void CommandToExecute()
    {}
};

#endif // MOVE_TO_STATE_H