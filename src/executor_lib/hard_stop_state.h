#ifndef THREAD_HARD_STOP_H
#define THREAD_HARD_STOP_H

#include "executor_state.h"
#include "state_exception.h"
#include "command.h"

class HardStopState : public State
{
public:
    HardStopState(ThreadQueue& queue) :
        State(queue)
    {}

private:
    p_state_t Strategy() override
    {
        return p_state_t(nullptr);
    }
};

class ThreadHardStopException : public StateException
{
public:
    ThreadHardStopException(const std::string& what) :
        StateException(what)
    {}

    p_state_t GetState(ThreadQueue& queue) const override
    {
        return std::make_shared<HardStopState>(queue);
    }
};

#define pThreadHardStopCommand std::make_shared<ThreadHardStopCommand>

class ThreadHardStopCommand : public Command
{
public:
    ThreadHardStopCommand()
    {}

    void Execute() override
    {
        throw ThreadHardStopException("Hard stop state exception was thrown");
    }

private:
    void CommandToExecute()
    {}
};

#endif // THREAD_HARD_STOP_H