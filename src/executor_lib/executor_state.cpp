#include "executor_state.h"
#include "state_exception.h"

State::State(ThreadQueue& queue) :
    m_queue(queue)
{}

State::~State()
{}

p_state_t State::Next()
{
    try
    {
        return Strategy();
    }
    catch(const StateException& e)
    {
        return e.GetState(m_queue);
    }
}