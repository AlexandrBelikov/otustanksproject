#ifndef CHANGE_VELOCITY_COMMAND_H
#define CHANGE_VELOCITY_COMMAND_H

#include <cmath>
#include "command.h"
#include "velocity_changeable_adapter.h"

#define pChangeVelocityCommand std::make_shared<ChangeVelocityCommand>

#if VECTOR_SIZE == 2

class ChangeVelocityCommand : public Command
{
public:
    ChangeVelocityCommand(IVelocityChangeable& velocityChangeable) :
        m_velocityChangeable(velocityChangeable)
    {}

private:
    IVelocityChangeable& m_velocityChangeable;

    void CommandToExecute()
    {
        int direction_delta = abs(m_velocityChangeable.GetInitialDirection() -
            m_velocityChangeable.GetDirection());
        double angle = (double)direction_delta * 2 * M_PI / 
            m_velocityChangeable.GetMaxDirections();
        double velocity = m_velocityChangeable.GetVelocity();
        Vector result({velocity * std::cos(angle), velocity * std::sin(angle)});
        m_velocityChangeable.SetVelocityVector(result);
    }
};

#endif

#endif // CHANGE_VELOCITY_COMMAND_H