#ifndef ROTATE_CHANGE_VELOCITY_COMMAND_H
#define ROTATE_CHANGE_VELOCITY_COMMAND_H

#include "command.h"
#include "check_fuel_command.h"
#include "rotate_command.h"
#include "burn_fuel_command.h"
#include "change_velocity_command.h"
#include "macro_command.h"

#define pRotateChangeVelocityCommand std::make_shared<RotateChangeVelocityCommand>

class RotateChangeVelocityCommand : public Command
{
public:
    RotateChangeVelocityCommand(IVelocityChangeable& velocityChangeable, 
        IRotable& rotable) :
        m_macroCommand(pMacroCommand(
            pRotateCommand(rotable),
            pChangeVelocityCommand(velocityChangeable)
        ))
    {}

private:
    p_cmd_t m_macroCommand;

    void CommandToExecute()
    {
        m_macroCommand->Execute();
    }
};

#endif // ROTATE_CHANGE_VELOCITY_COMMAND_H