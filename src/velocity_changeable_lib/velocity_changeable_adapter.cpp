#include <stdexcept>
#include "velocity_changeable_adapter.h"

const std::string VelocityChangeableAdapter::m_directionName = UPROP_DIRECTION;
const std::string VelocityChangeableAdapter::m_initialDirectionName = UPROP_INITIAL_DIRECTION;
const std::string VelocityChangeableAdapter::m_velocityVectorName = UPROP_VELOCITY_VECTOR;
const std::string VelocityChangeableAdapter::m_velocityName = UPROP_VELOCITY;
const std::string VelocityChangeableAdapter::m_maxDirectionsName = UPROP_MAX_DIRECTIONS;

int VelocityChangeableAdapter::GetInitialDirection() const
{
    return UCast(int, m_obj[m_initialDirectionName]);
}

int VelocityChangeableAdapter::GetDirection() const
{
    return UCast(int, m_obj[m_directionName]);
}

int VelocityChangeableAdapter::GetVelocity() const
{
    return UCast(int, m_obj[m_velocityName]);
}

int VelocityChangeableAdapter::GetMaxDirections() const
{
    return UCast(int, m_obj[m_maxDirectionsName]);
}

void VelocityChangeableAdapter::SetVelocityVector(const Vector& newVelocity)
{
    m_obj[m_velocityVectorName] = newVelocity;
}

void VelocityChangeableAdapter::SetInitialDirection(int newDir)
{
    m_obj[m_initialDirectionName] = newDir;
}