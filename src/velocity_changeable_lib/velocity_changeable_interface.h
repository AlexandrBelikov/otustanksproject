#ifndef VELOCITY_CHANGEABLE_INTERFACE_H
#define VELOCITY_CHANGEABLE_INTERFACE_H

#include "vector.h"

class IVelocityChangeable
{
public:
    virtual int GetInitialDirection() const = 0;
    virtual int GetDirection() const = 0;
    virtual int GetVelocity() const = 0;
    virtual int GetMaxDirections() const = 0;
    virtual void SetVelocityVector(const Vector& newVelocity) = 0;
    virtual ~IVelocityChangeable(){};

private:
    virtual void SetInitialDirection(int newDir) = 0;
};

#endif // VELOCITY_CHANGEABLE_INTERFACE_H