#ifndef VELOCITY_CHANGEABLE_ADAPTER_H
#define VELOCITY_CHANGEABLE_ADAPTER_H

#include "uobject.h"
#include "velocity_changeable_interface.h"

class VelocityChangeableAdapter : public IVelocityChangeable
{
public:
    static const std::string m_directionName;
    static const std::string m_initialDirectionName;
    static const std::string m_velocityVectorName;
    static const std::string m_velocityName;
    static const std::string m_maxDirectionsName;

    VelocityChangeableAdapter(UObject& obj) :
        m_obj(obj)
    {}

    int GetInitialDirection() const override;
    int GetDirection() const override;
    int GetVelocity() const override;
    int GetMaxDirections() const override;
    void SetVelocityVector(const Vector& newVelocity) override;

private:
    UObject& m_obj;

    void SetInitialDirection(int newDir) override;
};

#endif // VELOCITY_CHANGEABLE_ADAPTER_H