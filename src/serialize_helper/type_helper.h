#ifndef TYPE_HELPER_H
#define TYPE_HELPER_H

#include <string>
#include <vector>
#include "vector.h"
#include "uobject.h"

#define UNKNOWN_HASH "unknown_hash"

typedef std::pair<std::string, std::size_t> TypePair;
typedef std::vector<TypePair> TypeSet;

class TypeHelper
{
public:
    static const std::size_t m_intHash;
    static const std::size_t m_uintHash;
    static const std::size_t m_doubleHash;
    static const std::size_t m_floatHash;
    static const std::size_t m_boolHash;
    static const std::size_t m_strHash;
    static const std::size_t m_cstrHash;
    static const std::size_t m_uobjHash;
    static const std::size_t m_vectorHash;
    static const TypeSet     m_typenameMap;
    static const std::size_t m_unknownHash;
    static const std::string m_unknownHashName;

    static const TypeHelper& Get()
    {
        static const TypeHelper helper;
        return helper;
    }

    const std::string GetTypeName(std::size_t type_hash) const
    {
        for (TypePair pair : m_typenameMap)
        {
            if (type_hash == pair.second)
            {
                return pair.first;
            }
        }
        return m_unknownHashName;
    }

    const std::size_t GetHash(std::string& type_name) const
    {
        for (TypePair pair : m_typenameMap)
        {
            if (type_name == pair.first)
            {
                return pair.second;
            }
        }
        return m_unknownHash;
    }

private:
    TypeHelper()
    {}
};

#endif // TYPE_HELPER_H