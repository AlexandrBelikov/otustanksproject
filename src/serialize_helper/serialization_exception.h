#ifndef SERIALIZATION_EXCEPTION_H
#define SERIALIZATION_EXCEPTION_H

#include <exception>
#include "command_exception.h"

class SerializationException : public CommandException
{
public:
    SerializationException(const std::string& error = "") :
        CommandException(error)
    {}
};

#endif // SERIALIZATION_EXCEPTION_H