#include <sstream>
#include <iostream>

#include "serialize_helper.h"
#include "serialization_exception.h"
#include "type_helper.h"

std::string serialize_helper::arg_node_name(int index)
{
    static std::string name("arg_");
    return name + std::to_string(index);
}

std::string serialize_helper::vector_body_name(int index)
{
    static std::string name("vector_body_");
    return name + std::to_string(index);
}

void serialize_helper::Serialize(xml::xml_node& parent_node, p_serial_cmd_t cmd)
{
    cmd->Serialize(parent_node);
}

void serialize_helper::Serialize(xml::xml_node& parent_node, const ioc_args_t& dep_args)
{
    int counter = 0;

    for (std::any arg : dep_args)
    {
        xml::xml_node arg_node = parent_node.append_child(arg_node_name(counter++).c_str());
        Serialize(arg_node, arg);
    }
}

void serialize_helper::Serialize(xml::xml_node& parent_node, const std::any& arg)
{
    const std::size_t arg_hash = arg.type().hash_code();
    std::string type;
    std::stringstream value;
    const TypeHelper& type_helper = TypeHelper::Get();

    if (arg_hash == type_helper.m_intHash)
    {
        value << std::any_cast<int>(arg);
    }
    else if (arg_hash == type_helper.m_uintHash)
    {
        value << std::any_cast<uint>(arg);
    }
    else if (arg_hash == type_helper.m_doubleHash)
    {
        value << std::any_cast<double>(arg);
    }
    else if (arg_hash == type_helper.m_floatHash)
    {
        value << std::any_cast<float>(arg);
    }
    else if (arg_hash == type_helper.m_boolHash)
    {
        value << std::any_cast<bool>(arg);
    }
    else if (arg_hash == type_helper.m_strHash || arg_hash == type_helper.m_cstrHash)
    {
        value << std::any_cast<std::string>(arg);
    }
    else if (arg_hash == type_helper.m_uobjHash)
    {
        Serialize(parent_node, std::any_cast<UObject>(arg));
    }
    else if (arg_hash == type_helper.m_vectorHash)
    {
        Serialize(parent_node, std::any_cast<Vector>(arg));
    }
    else
    {
        throw SerializationException("Serialization type deduction is failed");
    }

    if (!value.str().empty())
    {
        parent_node.append_attribute("value") = value.str().c_str();
    }
    parent_node.append_attribute("type") = type_helper.GetTypeName(arg_hash).c_str();
}

void serialize_helper::Serialize(xml::xml_node& parent_node, const UObject& obj)
{
    for (uobj_body_t::const_iterator cit = obj.GetBody().begin(); cit != obj.GetBody().end(); cit++)
    {
        xml::xml_node node = parent_node.append_child("uobj_param");
        node.append_attribute("name") = cit->first.c_str();
        Serialize(node, cit->second);
    }
}

void serialize_helper::Serialize(xml::xml_node& parent_node, const Vector& vector)
{
    for (int index = 0; index < VECTOR_SIZE; ++index)
    {
        xml::xml_node node = parent_node.append_child(vector_body_name(index).c_str());
        node.append_attribute("value") = std::to_string(vector.GetBody(index)).c_str();
    }
}
