#ifndef SERIALIZE_HELPER_H
#define SERIALIZE_HELPER_H

#include <any>

#include "pugixml.hpp"
#include "serializable_command_interface.h"
#include "ioc_arguments.h"
#include "thread_queue.h"
#include "uobject.h"
#include "vector.h"

namespace serialize_helper
{
    std::string vector_body_name(int index);
    std::string arg_node_name(int index);

    void Serialize(xml::xml_node& parent_node, const p_serial_cmd_t cmd);
    void Serialize(xml::xml_node& parent_node, const ioc_args_t& dep_args);
    void Serialize(xml::xml_node& parent_node, const std::any& arg);
    void Serialize(xml::xml_node& parent_node, const UObject& obj);
    void Serialize(xml::xml_node& parent_node, const Vector& vector);
}


#endif // SERIALIZE_HELPER_H