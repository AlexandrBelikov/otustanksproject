#include "type_helper.h"

const std::size_t TypeHelper::m_intHash    = typeid(int).hash_code();
const std::size_t TypeHelper::m_uintHash   = typeid(uint).hash_code();
const std::size_t TypeHelper::m_doubleHash = typeid(double).hash_code();
const std::size_t TypeHelper::m_floatHash  = typeid(float).hash_code();
const std::size_t TypeHelper::m_boolHash   = typeid(bool).hash_code();
const std::size_t TypeHelper::m_strHash    = typeid(std::string).hash_code();
const std::size_t TypeHelper::m_cstrHash   = typeid(const char*).hash_code();
const std::size_t TypeHelper::m_uobjHash   = typeid(UObject).hash_code();
const std::size_t TypeHelper::m_vectorHash = typeid(Vector).hash_code();
const std::size_t TypeHelper::m_unknownHash = 0;
const std::string TypeHelper::m_unknownHashName = "unknown_hash";

const TypeSet TypeHelper::m_typenameMap = {
    {"int",     m_intHash},
    {"uint",    m_uintHash},
    {"double",  m_doubleHash},
    {"float",   m_floatHash},
    {"bool",    m_boolHash},
    {"string",  m_strHash},
    {"string",  m_cstrHash},
    {"uobject", m_uobjHash},
    {"vector",  m_vectorHash}
};