#ifndef IOC_EXCEPTION_H
#define IOC_EXCEPTION_H

#include <string>
#include <exception>

class IocException : public std::exception
{
public:
    IocException(const std::string& error = "") :
        m_error(error)
    {}

    const char* what() const noexcept
    {
        return m_error.c_str();
    }

private:
    std::string m_error;
};


#endif // IOC_EXCEPTION_H