#ifndef IOC_H
#define IOC_H

#include <vector>
#include <any>
#include <string>
#include <iostream>

#include "ioc_arguments.h"
#include "ioc_dependencies.h"
#include "ioc_exception.h"
#include "scope.h"
#include "lambda_command.h"

class IoC
{
public:
    template<typename T>
    static T Resolve(const std::string& dep_name, const ioc_args_t& dep_args = {})
    {
        try
        {
            return std::any_cast<T>(resolve_dependency(dep_name, dep_args));
        }
        catch(const IocException& e)
        {
            throw;
        }
        catch(const std::exception& e)
        {
            throw IocException(e.what());
        }
    }

private:
    IoC()
    {}
};

#endif // IOC_H