#include "ioc_dependencies.h"

namespace IOC
{
    std::string IoC("IoC");

    const std::string Register(IoC + ".Register");
    const std::string Adapter(IoC + ".Adapter");
    const std::string XmlInterpreter(IoC + ".XmlInterpreter");

    namespace Scope
    {
        std::string Scope("Scope");

        const std::string SuperScopeId(Scope + ".SuperScopeId");
        const std::string Set(Scope + ".Set");
        const std::string New(Scope + ".New");
        const std::string Current(Scope + ".Current");
        const std::string SetParent(Scope + ".SetParent");
    }
}