#ifndef IOC_ARGUMENTS_H
#define IOC_ARGUMENTS_H

#include <vector>
#include <any>
#include <sstream>
#include <algorithm>
#include "ioc_exception.h"

typedef std::vector<std::any> ioc_args_t;

void compare(const std::type_info& tpl_type, const ioc_args_t& args,
    std::ostringstream& error, size_t counter);

// recursive case
template<std::size_t N, typename T, typename... types>
class compare_type_recursively
{
public:
    compare_type_recursively(const ioc_args_t& args,
        std::ostringstream& error, size_t& counter)
    {
        compare(typeid(T), args, error, counter);
        compare_type_recursively<sizeof...(types), types...>(args, error, ++counter);
    }
};

// base case
template<typename T, typename... types>
class compare_type_recursively<1, T, types...>
{
public:
    compare_type_recursively(const ioc_args_t& args,
        std::ostringstream& error, size_t& counter)
    {
        compare(typeid(T), args, error, counter);
        while (++counter < args.size())
        {
            error << "Passed extra argument (" << counter << 
                ") of type: " << args[counter].type().name() << std::endl;
        }
    }
};

template<typename... Types>
void validate_ioc_args(const ioc_args_t& args)
{
    size_t types_size = sizeof...(Types);
    std::ostringstream error;

    if (args.size() != types_size)
    {
        error << "Invalid number of parameters: " << args.size()
            << "; Expected: " << types_size << std::endl;
    }

    size_t counter = 0;
    compare_type_recursively<sizeof...(Types), Types...>(args, error, counter);

    if (!error.str().empty())
    {
        throw IocException(error.str());
    }
}

#endif // IOC_ARGUMENTS_H