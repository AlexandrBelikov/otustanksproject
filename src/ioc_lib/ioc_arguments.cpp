#include "ioc_arguments.h"
#include <typeinfo>

class ArgNotPassed
{
public:
    ArgNotPassed()
    {}
};

void compare(const std::type_info& tpl_type, const ioc_args_t& args,
    std::ostringstream& error, size_t counter)
{
    const std::type_info& arg_type = (counter < args.size()) ? 
        args[counter].type() : typeid(ArgNotPassed);

    if (arg_type.hash_code() != tpl_type.hash_code())
    {
        error << "Parameter " << counter << " has incorrect type: "
            << arg_type.name() << " instead of "
            << tpl_type.name() << std::endl;
    }
}