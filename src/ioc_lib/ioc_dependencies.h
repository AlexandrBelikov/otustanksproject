#ifndef IOC_DEPENDENCIES_H
#define IOC_DEPENDENCIES_H

#include <string>

namespace IOC
{
    extern const std::string Register;
    extern const std::string Adapter;
    extern const std::string XmlInterpreter;

    namespace Scope
    {
        extern const std::string SuperScopeId;
        extern const std::string Set;
        extern const std::string New;
        extern const std::string Current;
        extern const std::string SetParent;
    }
}

#endif // IOC_DEPENDENCIES_H