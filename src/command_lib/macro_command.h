#ifndef MACRO_COMMAND_H
#define MACRO_COMMAND_H

#include <vector>
#include "command.h"

typedef std::vector<p_cmd_t> cmd_arr_t;
#define pMacroCommand(...) std::make_shared<MacroCommand>(cmd_arr_t{__VA_ARGS__})

class MacroCommand : public Command
{
public:
    MacroCommand(const cmd_arr_t& cmd_arr) :
        m_cmd_arr(cmd_arr)
    {}

    MacroCommand& Push(p_cmd_t cmd)
    {
        m_cmd_arr.push_back(cmd);
        return *this;
    }
    
private:
    cmd_arr_t m_cmd_arr;

    void CommandToExecute() override
    {
        for (p_cmd_t cmd : m_cmd_arr)
        {
            cmd->Execute();
        }
    }
};

#endif // MACRO_COMMAND_H