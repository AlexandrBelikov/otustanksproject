#ifndef LAMBDA_COMMAND_H
#define LAMBDA_COMMAND_H

#include <functional>
#include "command.h"

class LambdaCommand;
typedef std::function<void (void)> lambda_func_t;
typedef std::shared_ptr<LambdaCommand> p_lambda_t;
#define pLambdaCommand std::make_shared<LambdaCommand>

class LambdaCommand : public Command
{
public:
    LambdaCommand(lambda_func_t cmd) :
        m_command(cmd)
    {}

private:
    lambda_func_t m_command;

    void CommandToExecute()
    {
        m_command();
    }
};

#endif // LAMBDA_COMMAND_H