#ifndef COMMAND_EXCEPTION_H
#define COMMAND_EXCEPTION_H

#include <exception>
#include <string>

class CommandException : public std::exception
{
public:
    CommandException(const std::string& error = "") :
        m_error(error)
    {}

    const char* what() const noexcept
    {
        return m_error.c_str();
    }

private:
    std::string m_error;
};

#endif // COMMAND_EXCEPTION_H