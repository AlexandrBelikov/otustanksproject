#ifndef COMMAND_H
#define COMMAND_H

#include <memory>
#include <stdexcept>
#include <any>
#include "command_exception.h"

class Command;
typedef std::shared_ptr<Command> p_cmd_t;

class Command
{
public:
    virtual void Execute()
    {
        try
        {
            CommandToExecute();
        }
        catch(const std::bad_any_cast& e)
        {
            throw CommandException("Bad cast during command execution!");
        }
        catch(const std::out_of_range& e)
        {
            throw CommandException("Out of range during command execution!");
        }
        catch(const CommandException& e)
        {
            throw;
        }
        catch(const std::exception& e)
        {
            throw CommandException(std::string("Error happened executing command!") +
                e.what());
        }
    }

    virtual ~Command(){};

private:
    virtual void CommandToExecute() = 0;
};

#endif // COMMAND_H