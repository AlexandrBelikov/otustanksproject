#ifndef XML_DESERIALIZER_H
#define XML_DESERIALIZER_H

#include "thread_queue.h"
#include "deserialize_helper.h"
#include "serializable_macro_command.h"

p_cmd_t deserialize_xml(xml::xml_node& command);

template <typename T>
T deserialize_command_set(xml::xml_node doc)
{
    T result;
    for (xml::xml_node command = doc.child("command"); command; command = command.next_sibling("command"))
    {
        result.Push(deserialize_xml(command));
    }
    return result;
}

p_cmd_t deserialize_xml(xml::xml_node& command)
{
    if (command.attribute("macro").as_bool())
    {
        return std::dynamic_pointer_cast<Command>(std::make_shared<SerializableMacroCommand>(
            deserialize_command_set<SerializableMacroCommand>(command)));
    }
    else
    {
        return deserialize_helper::DeserializeCommand(command);
    }
}

#endif // XML_DESERIALIZER_H