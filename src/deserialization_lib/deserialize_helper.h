#ifndef DESERIALIZE_HELPER_H
#define DESERIALIZE_HELPER_H

#include "pugixml.hpp"
#include "command.h"
#include "ioc_arguments.h"

#include "vector.h"
#include "uobject.h"

namespace xml = pugi;

namespace deserialize_helper
{
    p_cmd_t    DeserializeCommand(xml::xml_node& node);
    ioc_args_t DeserializeArgs(xml::xml_node&& dep_args);
    std::any   DeserializeAny(xml::xml_node& node);
    UObject    DeserializeUobject(xml::xml_node& node);
    Vector     DeserializeVector(xml::xml_node& node);
}

#endif // DESERIALIZE_HELPER_H