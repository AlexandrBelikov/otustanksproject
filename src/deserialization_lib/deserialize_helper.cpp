#include "deserialize_helper.h"
#include "serialize_helper.h"
#include "type_helper.h"
#include "deserialization_exception.h"
#include "serializable_command.h"

p_cmd_t deserialize_helper::DeserializeCommand(xml::xml_node& command)
{
    std::string dep_name(command.child("dep_name").attribute("value").as_string());
    ioc_args_t dep_args(DeserializeArgs(command.child("dep_args")));
    return std::dynamic_pointer_cast<Command>(std::make_shared<SerializableCommand>(dep_name, dep_args));
}

ioc_args_t deserialize_helper::DeserializeArgs(xml::xml_node&& dep_args)
{
    ioc_args_t result;
    for (int i = 0; true; ++i)
    {
        xml::xml_node argument = dep_args.child(serialize_helper::arg_node_name(i).c_str());
        if (!argument)
        {
            break;
        }

        result.push_back(DeserializeAny(argument));
    }
    return result;
}

UObject deserialize_helper::DeserializeUobject(xml::xml_node& node)
{
    uobj_body_t result;
    for (xml::xml_node uobj_param = node.child("uobj_param"); uobj_param; uobj_param = uobj_param.next_sibling("uobj_param"))
    {
        result.insert(std::make_pair(uobj_param.attribute("name").as_string(), DeserializeAny(uobj_param)));
    }
    return result;
}

Vector deserialize_helper::DeserializeVector(xml::xml_node& node)
{
    vector_body_t result;
    
    for (int i = 0; i < VECTOR_SIZE; ++i)
    {
        std::string name(serialize_helper::vector_body_name(i));
        result[i] = node.child(name.c_str()).attribute("value").AS_VECTOR_BODY_TYPE();
    }
    return result;
}

std::any deserialize_helper::DeserializeAny(xml::xml_node& node)
{
    std::string type(node.attribute("type").as_string());
    const TypeHelper& type_helper = TypeHelper::Get();
    const std::size_t arg_hash = type_helper.GetHash(type);

    if (arg_hash == type_helper.m_intHash)
    {
        return node.attribute("value").as_int();
    }
    else if (arg_hash == type_helper.m_uintHash)
    {
        return node.attribute("value").as_uint();
    }
    else if (arg_hash == type_helper.m_doubleHash)
    {
        return node.attribute("value").as_double();
    }
    else if (arg_hash == type_helper.m_floatHash)
    {
        return node.attribute("value").as_float();
    }
    else if (arg_hash == type_helper.m_boolHash)
    {
        return node.attribute("value").as_bool();
    }
    else if (arg_hash == type_helper.m_strHash || arg_hash == type_helper.m_cstrHash)
    {
        return std::string(node.attribute("value").as_string());
    }
    else if (arg_hash == type_helper.m_uobjHash)
    {
        return DeserializeUobject(node);
    }
    else if (arg_hash == type_helper.m_vectorHash)
    {
        return DeserializeVector(node);
    }
    else
    {
        throw DeserializationException("Deserialization type deduction failed!");
    }
}