#ifndef FUEL_BURNABLE_INTERFACE_H
#define FUEL_BURNABLE_INTERFACE_H

class IFuelBurnable
{
public:
    virtual int GetFuelConsumption() const = 0;
    virtual int GetFuel() const = 0;
    virtual void SetFuel(int fuel_amount) = 0;
    virtual ~IFuelBurnable(){};
};

#endif // FUEL_BURNABLE_INTERFACE_H