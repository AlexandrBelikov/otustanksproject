#ifndef CHECK_FUEL_COMMAND_H
#define CHECK_FUEL_COMMAND_H

#include "command.h"
#include "fuel_burnable_interface.h"

#define pCheckFuelCommand std::make_shared<CheckFuelCommand>

class CheckFuelException : public CommandException
{
public:
    CheckFuelException(const std::string& error = "") :
        CommandException(error)
    {}
};

class CheckFuelCommand : public Command
{
public:
    CheckFuelCommand(IFuelBurnable& checkable) :
        m_checkable(checkable)
    {}

private:
    IFuelBurnable& m_checkable;

    void CommandToExecute()
    {
        if (m_checkable.GetFuel() < m_checkable.GetFuelConsumption())
        {
            throw CheckFuelException();
        }
    }
};

#endif // CHECK_FUEL_COMMAND_H