#include "fuel_burnable_adapter.h"

const std::string FuelBurnableAdapter::m_consumptionName = UPROP_FUEL_CONSUMPTION;
const std::string FuelBurnableAdapter::m_fuelAmountName = UPROP_FUEL_AMOUNT;

int FuelBurnableAdapter::GetFuelConsumption() const
{
    return UCast(int, m_obj[m_consumptionName]);
}

int FuelBurnableAdapter::GetFuel() const
{
    return UCast(int, m_obj[m_fuelAmountName]);
}

void FuelBurnableAdapter::SetFuel(int fuel_amount)
{
    m_obj[m_fuelAmountName] = fuel_amount;
}