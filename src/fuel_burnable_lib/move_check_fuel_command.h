#ifndef MOVE_CHECK_FUEL_COMMAND_H
#define MOVE_CHECK_FUEL_COMMAND_H

#include "command.h"
#include "movable_interface.h"
#include "fuel_burnable_interface.h"
#include "macro_command.h"
#include "move_command.h"
#include "check_fuel_command.h"
#include "burn_fuel_command.h"

#define pMoveCheckFuelCommand std::make_shared<MoveCheckFuelCommand>

class MoveCheckFuelCommand : public Command
{
public:
    MoveCheckFuelCommand(IMovable& movable, IFuelBurnable& burnable) :
        m_cmd(pMacroCommand(
            pCheckFuelCommand(burnable),
            pMoveCommand(movable),
            pBurnFuelCommand(burnable)
        ))
    {}

private:
    p_cmd_t m_cmd;

    void CommandToExecute() override
    {
        m_cmd->Execute();
    }
};

#endif // MOVE_CHECK_FUEL_COMMAND_H