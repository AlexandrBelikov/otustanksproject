#ifndef FUEL_BURNABLE_ADAPTER
#define FUEL_BURNABLE_ADAPTER

#include "uobject.h"
#include "fuel_burnable_interface.h"

class FuelBurnableAdapter : public IFuelBurnable
{
public:
    static const std::string m_consumptionName;
    static const std::string m_fuelAmountName;

    FuelBurnableAdapter(UObject& obj) :
        m_obj(obj)
    {}

    int GetFuelConsumption() const override;
    int GetFuel() const override;
    void SetFuel(int fuel_amount) override;

private:
    UObject& m_obj;
};

#endif // FUEL_BURNABLE_ADAPTER