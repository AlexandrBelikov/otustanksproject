#ifndef BURN_FUEL_COMMAND
#define BURN_FUEL_COMMAND

#include "command.h"
#include "fuel_burnable_adapter.h"

#define pBurnFuelCommand std::make_shared<BurnFuelCommand>

class BurnFuelCommand : public Command
{
public:
    BurnFuelCommand(IFuelBurnable& burnable) :
        m_burnable(burnable)
    {}

private:
    IFuelBurnable& m_burnable;

    void CommandToExecute()
    {
        double diff = m_burnable.GetFuel() - m_burnable.GetFuelConsumption();
        // Prevent negative fuel amount
        m_burnable.SetFuel(diff >= 0 ? diff : 0);
    }
};

#endif // BURN_FUEL_COMMAND