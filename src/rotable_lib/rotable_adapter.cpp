#include "rotable_adapter.h"

const std::string RotableAdapter::m_directionName = UPROP_DIRECTION;
const std::string RotableAdapter::m_angularVelocityName = UPROP_ANGULAR_VELOCITY;
const std::string RotableAdapter::m_maxDirectionsName = UPROP_MAX_DIRECTIONS;

int RotableAdapter::GetDirection() const
{
    return UCast(int, m_obj[m_directionName]);
}

int RotableAdapter::GetAngularVelocity() const
{
    return UCast(int, m_obj[m_angularVelocityName]);
}

int RotableAdapter::GetMaxDirections() const
{
    return UCast(int, m_obj[m_maxDirectionsName]);
}

void RotableAdapter::SetDirection(int newDir)
{
    m_obj[m_directionName] = newDir;
}