#ifndef ROTATE_COMMAND_H
#define ROTATE_COMMAND_H

#include "command.h"
#include "rotable_interface.h"

#define pRotateCommand std::make_shared<RotateCommand>

class RotateCommand : public Command
{
public:
    RotateCommand(IRotable &rotable) :
        m_rotable(rotable)
    {}

private:
    IRotable& m_rotable;

    void CommandToExecute()
    {
        m_rotable.SetDirection((m_rotable.GetDirection() +
            m_rotable.GetAngularVelocity()) % m_rotable.GetMaxDirections());
    }
};

#endif // ROTATE_COMMAND_H