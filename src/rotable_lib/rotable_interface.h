#ifndef ROTABLE_INTERFACE_H
#define ROTABLE_INTERFACE_H

class IRotable
{
public:
    virtual int GetDirection() const = 0;
    virtual void SetDirection(int newDir) = 0;
    virtual int GetAngularVelocity() const = 0;
    virtual int GetMaxDirections() const = 0;
    virtual ~IRotable(){};
};

#endif // ROTABLE_INTERFACE_H