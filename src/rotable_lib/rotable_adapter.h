#ifndef ROTABLE_ADAPTER_H
#define ROTABLE_ADAPTER_H

#include "rotable_interface.h"
#include "uobject.h"

class RotableAdapter : public IRotable
{
public:
    static const std::string m_directionName;
    static const std::string m_angularVelocityName;
    static const std::string m_maxDirectionsName;

    RotableAdapter(UObject& obj) :
       m_obj(obj)
    {}

    int GetDirection() const;
    int GetAngularVelocity() const;
    int GetMaxDirections() const;
    void SetDirection(int newDir);

private:
    UObject& m_obj;
};

#endif // ROTABLE_ADAPTER_H