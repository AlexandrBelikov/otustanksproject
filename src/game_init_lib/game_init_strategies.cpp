#include "game_init_strategies.h"
#include "ioc.h"
#include "vector.h"
#include "uobject.h"

namespace GameInitStrategies
{
    p_lambda_t register_game_parameters(UObject& game_params)
    {
        return IoC::Resolve<p_lambda_t>(IOC::Register, {GameInitCommons::game_parameters_dep, scope_strategy_t(STRAT_ARGS_MUTABLE(game_params){
            return std::ref(game_params);
        })});
    }

    p_lambda_t register_new_tank_dep()
    {
        return IoC::Resolve<p_lambda_t>(IOC::Register, {GameInitCommons::new_tank_dep, scope_strategy_t(STRAT_ARGS_CAPTURE(){
            return UObject();
        })});
    }

    p_lambda_t register_user_for_tank_dep()
    {
        return IoC::Resolve<p_lambda_t>(IOC::Register, {GameInitCommons::set_user_for_tank_dep, scope_strategy_t(STRAT_ARGS_CAPTURE(){
            validate_ioc_args<REF(UObject)>(args);
            return pLambdaCommand([&](){
                UObject& tank = UCast(REF(UObject), args[0]);

                tank.Insert(GameInitCommons::tank_user_name, (UCast(int, GameInitCommons::get_game_params()[GameInitCommons::game_curr_tank_param]) < 
                    (GameInitCommons::get_max_tanks() / 2) ? GameInitCommons::user1_name : GameInitCommons::user2_name));
            });
        })});
    }

    p_lambda_t register_set_coord_dep()
    {
        return IoC::Resolve<p_lambda_t>(IOC::Register, {GameInitCommons::set_tank_coord_dep, scope_strategy_t(STRAT_ARGS_CAPTURE(){
            validate_ioc_args<REF(UObject)>(args);
            return pLambdaCommand([&](){
                UObject& tank = UCast(REF(UObject), args[0]);

                Vector game_field_size = UCast(Vector, GameInitCommons::get_game_params()[GameInitCommons::game_field_size_param]);
                    
                vector_val_t x_max_pos = game_field_size.GetBody(Vector::Dimensions::DIM_X);
                vector_val_t y_max_pos = game_field_size.GetBody(Vector::Dimensions::DIM_Y);
                vector_val_t y_pos = y_max_pos * (GameInitCommons::is_first_player() ? 1 : 2) / 3;

                int user_max_tanks = GameInitCommons::get_max_tanks() / 2;
                int user_tank_number = UCast(int, GameInitCommons::get_game_params()[GameInitCommons::game_curr_tank_param]) % user_max_tanks;
                vector_val_t x_pos = user_tank_number * x_max_pos / user_max_tanks;
                tank.Insert(UPROP_POSITION, Vector({x_pos, y_pos}));
            });
        })});
    }

    p_lambda_t register_set_direction_dep()
    {
        return IoC::Resolve<p_lambda_t>(IOC::Register, {GameInitCommons::set_tank_direction, scope_strategy_t(STRAT_ARGS_CAPTURE(){
            validate_ioc_args<REF(UObject)>(args);
            return pLambdaCommand([&](){
                UObject& tank = UCast(REF(UObject), args[0]);

                tank.Insert(UPROP_DIRECTION, GameInitCommons::is_first_player() ? Vector({0.0, 1.0}) : Vector({0.0, -1.0}));
            });
        })});
    }
}