#include "game_init_commons.h"
#include "ioc.h"

namespace GameInitCommons
{
    const std::string game_field_size_param("fiteldSize");
    const std::string game_curr_tank_param("currentTank");
    const std::string game_max_tanks_param("maxTanks");

    const std::string game_parameters_dep("gameParameters");
    const std::string new_tank_dep("newTankDep");
    const std::string set_user_for_tank_dep("setUser");
    const std::string set_tank_coord_dep("setCoord");
    const std::string set_tank_direction("setDirection");

    const std::string tank_user_name("username");
    const std::string user1_name("user1");
    const std::string user2_name("user2");

    void init_tank(UObject& tank, std::vector<std::string>& commands)
    {
        // Перебирая массив с именами команд выполняем инициализацию танка
        for (auto cmd : commands)
        {
            IoC::Resolve<p_lambda_t>(cmd, {std::ref(tank)})->Execute();
        }
    }

    UObject& get_game_params()
    {
        return IoC::Resolve<REF(UObject)>(game_parameters_dep);
    }

    int get_max_tanks()
    {
        return UCast(int, get_game_params()[game_max_tanks_param]);
    }

    bool is_first_player()
    {
        return UCast(int, get_game_params()[game_curr_tank_param]) < (get_max_tanks() / 2);
    }
}