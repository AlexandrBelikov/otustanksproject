#ifndef GAME_INIT_STRATEGIES_H
#define GAME_INIT_STRATEGIES_H

#include "lambda_command.h"
#include "game_init_commons.h"

namespace GameInitStrategies
{
    p_lambda_t register_game_parameters(UObject& game_params);
    p_lambda_t register_new_tank_dep();
    p_lambda_t register_user_for_tank_dep();
    p_lambda_t register_set_coord_dep();
    p_lambda_t register_set_direction_dep();
}

#endif // GAME_INIT_STRATEGIES_H