#ifndef GAME_INIT_COMMONS_H
#define GAME_INIT_COMMONS_H

#include <string>
#include <vector>
#include "uobject.h"

#define REF(x) std::reference_wrapper<x>

namespace GameInitCommons
{
    extern const std::string game_field_size_param;
    extern const std::string game_curr_tank_param;
    extern const std::string game_max_tanks_param;

    extern const std::string game_parameters_dep;
    extern const std::string new_tank_dep;
    extern const std::string set_user_for_tank_dep;
    extern const std::string set_tank_coord_dep;
    extern const std::string set_tank_direction;

    extern const std::string tank_user_name;
    extern const std::string user1_name;
    extern const std::string user2_name;

    void init_tank(UObject& tank, std::vector<std::string>& commands);
    UObject& get_game_params();
    int get_max_tanks();
    bool is_first_player();
}

#endif // GAME_INIT_COMMONS_H