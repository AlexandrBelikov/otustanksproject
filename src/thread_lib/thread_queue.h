#ifndef THREAD_QUEUE_H
#define THREAD_QUEUE_H

#include <thread>
#include <mutex>
#include <queue>
#include <chrono>
#include <any>
#include <condition_variable>

#include "command.h"

class ThreadQueue
{
public:
    ThreadQueue();
    ThreadQueue(const ThreadQueue& queue);
    
    // Sender's functions
    ThreadQueue& Push(p_cmd_t cmd);
    ThreadQueue& Push(ThreadQueue copy_from);
    void Notify();

    // Reciever's functions
    p_cmd_t Take();
    size_t Size();
    void Wait();

private:
    mutable std::mutex m_guard;
    std::queue<p_cmd_t> m_queue;
    p_cmd_t m_command;
    std::condition_variable m_notifier;
};

#endif // THREAD_QUEUE_H