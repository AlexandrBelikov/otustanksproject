#include <iostream>
#include "command_executor.h"
#include "regular_state.h"
#include "executor_state.h"
#include "ioc.h"

void CommandExecutor::ThreadBody()
{
    IoC::Resolve<p_lambda_t>(IOC::Scope::SetParent, {m_parentId})->Execute();

    while (!m_cancelled && m_state)
    {
        try
        {
            m_state = m_state->Next();
        }
        catch(const CommandException& e)
        {
            std::cerr << "CommandExecutor exception: " << e.what() << std::endl <<
                "Continuing thread execution " << std::this_thread::get_id << std::endl;
            continue;
        }
        catch(const std::exception& e)
        {
            std::cerr << "Unknowh exception was caught " << e.what() << std::endl <<
                "Stopping thread execution " << std::this_thread::get_id << std::endl;
        }
    }
}

CommandExecutor::CommandExecutor(ThreadQueue& queue) :
    m_parentId(IoC::Resolve<scope_id_t>(IOC::Scope::Current)),
    m_cancelled(false),
    m_queue(queue),
    m_state(std::make_shared<RegularState>(m_queue)),
    m_thread(&CommandExecutor::ThreadBody, this)
{}

CommandExecutor::~CommandExecutor()
{
    Dispose();
}

void CommandExecutor::Dispose()
{
    m_cancelled = true;
    m_queue.Notify();
    TryJoin();
}

void CommandExecutor::TryJoin()
{
    if (m_thread.joinable())
    {
        m_thread.join();
    }
}
