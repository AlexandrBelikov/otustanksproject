#include "thread_queue.h"

ThreadQueue::ThreadQueue()
{}

ThreadQueue::ThreadQueue(const ThreadQueue& queue)
{
    std::scoped_lock lock(queue.m_guard);
    this->m_queue = queue.m_queue;
}

p_cmd_t ThreadQueue::Take()
{
    std::scoped_lock lock(m_guard);
    if (!m_queue.empty())
    {
        m_command.swap(m_queue.front());
        m_queue.pop();
        return m_command;
    }
    else
    {
        return p_cmd_t(nullptr);
    }
}

ThreadQueue& ThreadQueue::Push(p_cmd_t cmd)
{
    std::scoped_lock lock(m_guard);
    m_queue.push(cmd);
    Notify();
    return *this;
}

ThreadQueue& ThreadQueue::Push(ThreadQueue copy_from)
{
    while (copy_from.Size())
    {
        this->Push(copy_from.Take());
    }
    return *this;
}

size_t ThreadQueue::Size()
{
    std::scoped_lock lock(m_guard);
    return m_queue.size();
}

void ThreadQueue::Wait()
{
    std::mutex stub_mutex;
    std::unique_lock stub_lock(stub_mutex);
    m_notifier.wait_for(stub_lock, std::chrono::milliseconds(100));
}

void ThreadQueue::Notify()
{
    m_notifier.notify_all();
}