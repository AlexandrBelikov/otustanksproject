#ifndef COMMAND_EXECUTOR_H
#define COMMAND_EXECUTOR_H

#include <atomic>
#include "thread_queue.h"
#include "executor_lib.h"
#include "scope_impl.h"

class CommandExecutor
{
public:
    CommandExecutor(ThreadQueue& queue);
    ~CommandExecutor();
    void Dispose();
    void TryJoin();

private:
    scope_id_t m_parentId;
    std::atomic_bool m_cancelled;
    ThreadQueue& m_queue;
    p_state_t m_state;
    std::thread m_thread;

    void ThreadBody();
};

#endif // COMMAND_EXECUTOR_H