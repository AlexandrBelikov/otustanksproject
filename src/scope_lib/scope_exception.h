#ifndef SCOPE_EXCEPTION_H
#define SCOPE_EXCEPTION_H

#include <string>
#include <exception>

class ScopeException : public std::exception
{
public:
    ScopeException(const std::string& error = "") :
        m_error(error)
    {}

    const char* what() const noexcept
    {
        return m_error.c_str();
    }

private:
    std::string m_error;
};

#endif // SCOPE_EXCEPTION_H