#ifndef SCOPE_H
#define SCOPE_H

#include "scope_impl.h"
#include "scope_manager.h"
#include "scope_exception.h"
#include "ioc_arguments.h"
#include "thread_local_container.h"

std::any resolve_dependency(const std::string& dep_name, const ioc_args_t& dep_args);

#endif // SCOPE_H
