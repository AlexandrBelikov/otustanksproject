#ifndef SCOPE_IMPL_H
#define SCOPE_IMPL_H

#include <string>
#include <functional>
#include <map>
#include <any>

#include "ioc_arguments.h"

#define STRAT_ARGS_CAPTURE(...) [__VA_ARGS__](const ioc_args_t &args)->std::any
#define STRAT_ARGS_MUTABLE(...) [__VA_ARGS__](const ioc_args_t &args)mutable->std::any

class Scope;
typedef std::string scope_id_t;
typedef std::function<std::any(const ioc_args_t&)> scope_strategy_t;
typedef std::map<std::string, scope_strategy_t> scope_map_t;

class Scope
{
friend class ScopeManager;

public:
    scope_id_t GetId();
    std::any Resolve(const std::string& dep_name, const ioc_args_t& dep_args);
    static scope_id_t GetSuperScope();
    Scope& SetStrategy(const std::string& dep_name, scope_strategy_t strategy);
    Scope& SetParent(const scope_id_t& scope_id);

private:
    scope_id_t m_id;
    scope_id_t m_parent;
    scope_map_t m_body;

    Scope(scope_id_t id, scope_id_t parent_id);
    scope_id_t GetParent();
    scope_strategy_t* GetStrategy(const std::string& dep_name);
};

#endif // SCOPE_IMPL_H