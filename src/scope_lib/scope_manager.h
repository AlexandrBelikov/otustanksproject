#ifndef SCOPE_MANAGER_H
#define SCOPE_MANAGER_H

#include <memory>
#include <map>
#include <mutex>

#include "scope_impl.h"
#include "scope_exception.h"

typedef std::shared_ptr<Scope> p_scope_t;
typedef std::map<std::string, p_scope_t> scope_list_t;

class ScopeManager
{
public:
    static ScopeManager& Get();
    scope_id_t GetNewScope(scope_id_t parent_id);
    p_scope_t GetScopeById(const scope_id_t& scope_id);

private:
    scope_list_t m_body;
    std::mutex m_guard;

    ScopeManager(){};
    static std::string GenerateId();
};

#endif // SCOPE_MANAGER_H