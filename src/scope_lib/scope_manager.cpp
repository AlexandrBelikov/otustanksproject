#include "scope_manager.h"

ScopeManager& ScopeManager::Get()
{
    static ScopeManager singleton;
    return singleton;
}

scope_id_t ScopeManager::GetNewScope(scope_id_t parent_id)
{
    scope_id_t new_id = GenerateId();
    Scope *p_new_scope = new Scope(new_id, parent_id);
    p_scope_t new_scope(p_new_scope);

    {
        std::scoped_lock lock(m_guard);
        m_body.insert_or_assign(new_id, new_scope);
    }
    return new_id;
}

p_scope_t ScopeManager::GetScopeById(const scope_id_t& scope_id)
{
    scope_list_t::iterator it;
    {
        std::scoped_lock lock(m_guard);
        it = m_body.find(scope_id);
    }

    if (it != m_body.end())
    {
        return it->second;
    }
    else
    {
        throw ScopeException("No scope found by id " + scope_id);
    }
}

std::string ScopeManager::GenerateId()
{
    static int counter = 0;
    return std::to_string(counter++);
}