#include "command.h"
#include "ioc.h"
#include "lambda_command.h"
#include "ioc_arguments.h"

Scope::Scope(scope_id_t id, scope_id_t parent) :
    m_id(id),
    m_parent(parent)
{}

scope_id_t Scope::GetId()
{
    return m_id;
}

std::any Scope::Resolve(const std::string& dep_name, const ioc_args_t& dep_args)
{
    Scope *scope_ptr = this;
    scope_strategy_t *func = nullptr;
    while (1)
    {
        func = scope_ptr->GetStrategy(dep_name);
        if (func)
        {
            break;
        }

        if (!scope_ptr->GetParent().empty())
        {
            scope_ptr = ScopeManager::Get().GetScopeById(scope_ptr->GetParent()).get();
        }
        else
        {
            throw ScopeException("No such dependency found: " + dep_name);
        }
    }
    
    return std::invoke(*func, dep_args);
}

Scope& Scope::SetStrategy(const std::string& dep_name, scope_strategy_t strategy)
{
    m_body.insert_or_assign(dep_name, strategy);
    return *this;
}

scope_id_t Scope::GetParent()
{
    return m_parent;
}

scope_strategy_t* Scope::GetStrategy(const std::string& dep_name)
{
    scope_map_t::iterator it = m_body.find(dep_name);
    if (it != m_body.end())
    {
        return &(it->second);
    }
    else
    {
        return nullptr;
    }
}

scope_id_t Scope::GetSuperScope()
{
    static bool superScopeExists = false;
    static p_scope_t superScope = ScopeManager::Get().GetScopeById(
        ScopeManager::Get().GetNewScope(""));

    if (superScopeExists)
    {
        return superScope->GetId();
    }
    superScopeExists = true;

    scope_id_t superScopeId = superScope->GetId();

    superScope
    ->SetStrategy(IOC::Scope::New, [](const ioc_args_t& args)->std::any
    {
        return ScopeManager::Get().GetNewScope(IoC::Resolve<scope_id_t>(IOC::Scope::Current));
    })

    .SetStrategy(IOC::Scope::Set, [](const ioc_args_t& args)->std::any
    {
        validate_ioc_args<scope_id_t>(args);
        return pLambdaCommand([args]()
        {
            ThreadLocalContainer::Get().SetScope(std::any_cast<scope_id_t>(args[0]));
        });
    })

    .SetStrategy(IOC::Scope::SuperScopeId, [superScopeId](const ioc_args_t& args)->std::any
    {
        return superScopeId;
    })

    .SetStrategy(IOC::Scope::Current, [](const ioc_args_t& args)->std::any
    {
        return ThreadLocalContainer::Get().GetScope();
    })

    .SetStrategy(IOC::Register, [](const ioc_args_t& args)->std::any
    {
        validate_ioc_args<std::string, scope_strategy_t>(args);
        return pLambdaCommand([args]()
        {
            scope_id_t current_scope = IoC::Resolve<scope_id_t>(IOC::Scope::Current);
            ScopeManager::Get().GetScopeById(current_scope)->SetStrategy(std::any_cast<std::string>(args[0]), 
                std::any_cast<scope_strategy_t>(args[1]));
        });
    })

    .SetStrategy(IOC::Scope::SetParent, [](const ioc_args_t& args)->std::any
    {
        validate_ioc_args<scope_id_t>(args);
        return pLambdaCommand([args](){
            scope_id_t current_scope = IoC::Resolve<scope_id_t>(IOC::Scope::Current);
            ScopeManager::Get().GetScopeById(current_scope)->SetParent(std::any_cast<scope_id_t>(args[0]));
        });
    });

    return superScopeId;
}

Scope& Scope::SetParent(const scope_id_t& scope_id)
{
    m_parent = scope_id;
    return *this;
}