#include "thread_local_container.h"
#include "lambda_command.h"
#include "ioc.h"

scope_id_t ThreadLocalContainer::GetScope()
{
    thread_id_t id = GetThreadId();
    try
    {
        {
            std::scoped_lock lock(m_guard);
            return m_map.at(id);
        }
    }
    catch(const std::exception& e)
    {
        {
            std::scoped_lock lock(m_guard);
            m_map.insert({GetThreadId(), Scope::GetSuperScope()});
        }
        scope_id_t newScope = IoC::Resolve<scope_id_t>(IOC::Scope::New);
        IoC::Resolve<p_lambda_t>(IOC::Scope::Set, {newScope})->Execute();

        return newScope;
    }
}

void ThreadLocalContainer::SetScope(scope_id_t another)
{
    thread_id_t id = GetThreadId();
    {
        std::scoped_lock lock(m_guard);
        m_map.at(id) = another;
    }
}

ThreadLocalContainer& ThreadLocalContainer::Get()
{
    static ThreadLocalContainer singleton;
    return singleton;
}

thread_id_t ThreadLocalContainer::GetThreadId()
{
    return std::this_thread::get_id();
}

ThreadLocalContainer::ThreadLocalContainer(){};