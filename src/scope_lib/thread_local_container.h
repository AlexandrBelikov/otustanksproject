#ifndef THREAD_LOCAL_CONTAINER_H
#define THREAD_LOCAL_CONTAINER_H

#include <map>
#include <thread>
#include <functional>
#include <mutex>
#include "scope_impl.h"

typedef std::thread::id thread_id_t;

class ThreadLocalContainer
{
public:
    scope_id_t GetScope();
    void SetScope(scope_id_t another);
    static ThreadLocalContainer& Get();

private:
    std::map<thread_id_t, scope_id_t> m_map;
    std::mutex m_guard;

    ThreadLocalContainer();
    thread_id_t GetThreadId();
};

#endif // THREAD_LOCAL_CONTAINER_H