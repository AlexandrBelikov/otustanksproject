#include "scope.h"

std::any resolve_dependency(const std::string& dep_name, const ioc_args_t& dep_args)
{
    return ScopeManager::Get().GetScopeById(ThreadLocalContainer::Get()
        .GetScope())->Resolve(dep_name, dep_args);
}