#ifndef SERIALIZABLE_COMMAND_H
#define SERIALIZABLE_COMMAND_H

#include "serializable_command_interface.h"
#include "ioc.h"
#include "serialize_helper.h"

#define pSerializableCommand(dep_name, ...) std::shared_ptr<SerializableCommand>(new SerializableCommand(dep_name, ioc_args_t(__VA_ARGS__) ))

class SerializableCommand : public ISerializableCommand
{
public:
    SerializableCommand(std::string dep_name, const ioc_args_t& dep_args = {}) :
        m_depName(dep_name),
        m_depArgs(dep_args)
    {}

    void Serialize(xml::xml_node& parent_node) const override
    {
        xml::xml_node dep_node = parent_node.append_child("dep_name");
        dep_node.append_attribute("value") = m_depName.c_str();
        dep_node.append_attribute("type") = "string";
        xml::xml_node dep_args_node = parent_node.append_child("dep_args");
        serialize_helper::Serialize(dep_args_node, m_depArgs);
    }

private:
    std::string m_depName;
    ioc_args_t m_depArgs;

    void CommandToExecute() override
    {
        IoC::Resolve<p_lambda_t>(m_depName, m_depArgs)->Execute();
    }
};

#endif // SERIALIZABLE_COMMAND_H