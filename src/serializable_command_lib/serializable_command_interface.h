#ifndef SERIALIZABLE_COMMAND_INTERFACE_H
#define SERIALIZABLE_COMMAND_INTERFACE_H

#include "command.h"
#include "pugixml.hpp"
namespace xml = pugi;

class ISerializableCommand : public Command
{
public:
    virtual void Serialize(xml::xml_node& parent_node) const = 0;
};

typedef std::shared_ptr<ISerializableCommand> p_serial_cmd_t;

#endif // SERIALIZABLE_COMMAND_INTERFACE_H