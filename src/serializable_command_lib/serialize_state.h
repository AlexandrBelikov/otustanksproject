#ifndef THREAD_SERIALIZE_STATE
#define THREAD_SERIALIZE_STATE

#include "executor_state.h"
#include "serializable_command_interface.h"
#include "serialize_helper.h"
#include "state_exception.h"
#include "exit_moveto_state.h"

typedef ThreadExitMoveToStateCommand ThreadExitSerializeStateCommand;
#define pThreadExitSerializeStateCommand pThreadExitMoveToStateCommand

class SerializeState : public State
{
public:
    SerializeState(ThreadQueue& queue, xml::xml_node& root_node) :
        State(queue),
        m_rootNode(root_node)
    {}

private:
    xml::xml_node& m_rootNode;

    p_state_t Strategy() override
    {
        p_cmd_t command = State::m_queue.Take();
        if (command)
        {
            if (dynamic_cast<ThreadExitSerializeStateCommand*>(command.get()))
            {
                command->Execute();
            }
            else if (dynamic_cast<ISerializableCommand*>(command.get()))
            {
                xml::xml_node cmd_node = m_rootNode.append_child("command");
                try
                {
                    serialize_helper::Serialize(cmd_node, std::dynamic_pointer_cast<ISerializableCommand>(command));
                }
                catch(const std::exception& e)
                {
                    m_rootNode.remove_child(cmd_node);
                }
            }
        }
        else
        {
            State::m_queue.Wait();
        }
        return shared_from_this();
    }
};

class SerializeStateException : public StateException
{
public:
    SerializeStateException(const std::string& what, xml::xml_node& root_node) :
        StateException(what),
        m_rootNode(root_node)
    {}

    p_state_t GetState(ThreadQueue& queue) const override
    {
        return std::make_shared<SerializeState>(queue, m_rootNode);
    }

private:
    xml::xml_node& m_rootNode;
};

#define pThreadSerializeCommand std::make_shared<ThreadSerializeCommand>

class ThreadSerializeCommand : public Command
{
public:
    ThreadSerializeCommand(xml::xml_node& root_node) :
        m_rootNode(root_node)
    {}

    void Execute() override
    {
        throw SerializeStateException("SerializeState exception was thrown", m_rootNode);
    }

private:
    xml::xml_node& m_rootNode;

    void CommandToExecute()
    {}
};

#endif // THREAD_SERIALIZE_STATE