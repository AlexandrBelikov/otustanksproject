#ifndef SERIALIZABLE_MACRO_COMMAND_H
#define SERIALIZABLE_MACRO_COMMAND_H

#include "serializable_command_interface.h"
#include "macro_command.h"
#include "serialize_helper.h"
#include "serialization_exception.h"

#define pSerializableMacroCommand(...) std::make_shared<SerializableMacroCommand>(cmd_arr_t{__VA_ARGS__})

class SerializableMacroCommand : public ISerializableCommand
{
public:
    SerializableMacroCommand()
    {}

    SerializableMacroCommand(const cmd_arr_t& cmd_arr) :
        m_cmdArr(cmd_arr)
    {}

    SerializableMacroCommand& Push(p_cmd_t cmd)
    {
        m_cmdArr.push_back(cmd);
        return *this;
    }

    void Serialize(xml::xml_node& parent_node) const override
    {
        parent_node.append_attribute("macro") = true;
        for (p_cmd_t cmd : m_cmdArr)
        {
            if (dynamic_cast<ISerializableCommand*>(cmd.get()))
            {
                xml::xml_node cmd_node = parent_node.append_child("command");
                serialize_helper::Serialize(cmd_node, std::dynamic_pointer_cast<ISerializableCommand>(cmd));
            }
            else
            {
                throw SerializationException("Non-serializable command found");
            }
        }
    }

private:
    cmd_arr_t m_cmdArr;

    void CommandToExecute() override
    {
        for (p_cmd_t cmd : m_cmdArr)
        {
            cmd->Execute();
        }
    }
};

#endif // SERIALIZABLE_MACRO_COMMAND_H