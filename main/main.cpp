#include <iostream>
#include <string>
#include <sstream>

#include "lambda_command.h"
#include "thread_queue.h"
#include "command_executor.h"
#include "uobject.h"
#include "rotable_adapter.h"
#include "movable_adapter.h"
#include "fuel_burnable_adapter.h"
#include "macro_command.h"
#include "move_check_fuel_command.h"
#include "rotate_change_velocity.h"
#include "ioc.h"
#include "ioc_dependencies.h"

#include "serializable_command.h"
#include "serialize_state.h"
#include "serializable_macro_command.h"
#include "xml_deserializer.h"

/**
 * Схема демонстрации: создаем очередь команд, сериализуем ее, затем
 * десериализуем обратно и исполняем, чтобы продемонстрировать корректное 
 * выполнение, а затем снова сериализуем и сравниваем результаты, чтобы
 * убедиться, что в процессе преобразований ничего не потерялось
 */

int main(int argc, char const *argv[])
{
    std::string xml_path("./main/test_xml.xml");

    // Создаем объект типа UObject
    UObject tank({
        {UPROP_POSITION, Vector({0.0, 0.0})},
        {UPROP_VELOCITY_VECTOR, Vector({3.0, 0.0})},
        {UPROP_DIRECTION, 0},
        {UPROP_INITIAL_DIRECTION, 0},
        {UPROP_ANGULAR_VELOCITY, 1},
        {UPROP_MAX_DIRECTIONS, 8},
        {UPROP_FUEL_CONSUMPTION, 1},
        {UPROP_FUEL_AMOUNT, 50},
        {UPROP_VELOCITY, 3}
    });

    // Определяем набор имен тестовых команд
    std::string test_cmd_0("TestCmd0");
    std::string test_cmd_1("TestCmd1");
    std::string test_cmd_2("TestCmd2");

    // Регистрируем зависимости тестовых команд
    IoC::Resolve<p_lambda_t>(IOC::Register, {test_cmd_0, scope_strategy_t(STRAT_ARGS_CAPTURE(&test_cmd_0){
        validate_ioc_args<double, std::string>(args);
        return pLambdaCommand([&test_cmd_0](){ std::cout << test_cmd_0 << " done" << std::endl; });
    })})->Execute();

    scope_id_t current_id_3 = IoC::Resolve<scope_id_t>(IOC::Scope::Current);

    IoC::Resolve<p_lambda_t>(IOC::Register, {test_cmd_1, scope_strategy_t(STRAT_ARGS_CAPTURE(&test_cmd_1){
        validate_ioc_args<int, double>(args);
        return pLambdaCommand([&test_cmd_1](){ std::cout << test_cmd_1 << " done" << std::endl; });
    })})->Execute();

    IoC::Resolve<p_lambda_t>(IOC::Register, {test_cmd_2, scope_strategy_t(STRAT_ARGS_CAPTURE(&test_cmd_2){
        validate_ioc_args<int, double, Vector, UObject>(args);
        return pLambdaCommand([&test_cmd_2](){ std::cout << test_cmd_2 << " done" << std::endl; });
    })})->Execute();

    // Регистрируем интерпретатор как зависимость IoC контейнера
    IoC::Resolve<p_lambda_t>(IOC::Register, {IOC::XmlInterpreter, scope_strategy_t(STRAT_ARGS_CAPTURE(){
        validate_ioc_args<xml::xml_node>(args);
        return deserialize_command_set<ThreadQueue>(std::any_cast<xml::xml_node>(args[0]));
    })})->Execute();

    // Получаем документ для работы и корневую ноду
    xml::xml_document doc;
    xml::xml_parse_result file_open_result = doc.load_file(xml_path.c_str());
    std::cout << "File open result: " << file_open_result << std::endl;
    xml::xml_node parent = doc.child("root");

    // Создаем исполнитель для сериализации команд
    ThreadQueue ser_queue;
    ser_queue.Push(pThreadSerializeCommand(parent))
             .Push(pSerializableCommand(test_cmd_0, {3.14, std::string("Hello") }))
             .Push(pSerializableMacroCommand(
                pSerializableCommand(test_cmd_1, {3, 1.14 }),
                pSerializableCommand(test_cmd_2, {3, 1.14, Vector({71, 17}), tank })
             ))
             .Push(pThreadExitSerializeStateCommand())
             .Push(pThreadHardStopCommand());

    CommandExecutor ser_thread(ser_queue);
    ser_thread.TryJoin();

    // Создаем тестовый выходной поток
    std::ostringstream result;
    doc.save(result);
    std::cout << "Serialization result: " << std::endl;
    doc.save(std::cout);

    // Десериализуем полученный xml
    ThreadQueue deserialize_queue = IoC::Resolve<ThreadQueue>(IOC::XmlInterpreter, {doc.child("root")});

    // Исполняем полученный xml
    std::cout << std::endl << "Execution result: " << std::endl;
    ThreadQueue exec_queue;
    exec_queue.Push(deserialize_queue)
              .Push(pThreadHardStopCommand());
    CommandExecutor executor(exec_queue);
    executor.TryJoin();
    std::cout << std::endl;
    
    // Получаем новую корневую ноду для работы
    xml::xml_document new_doc;
    new_doc.load_file(xml_path.c_str());
    xml::xml_node new_parent = new_doc.child("root");

    // Создаем новый исполнитель для обратной сериализации
    ThreadQueue new_queue;
    new_queue.Push(pThreadSerializeCommand(new_parent))
             .Push(deserialize_queue)
             .Push(pThreadExitSerializeStateCommand())
             .Push(pThreadHardStopCommand());

    CommandExecutor new_executor(new_queue);
    new_executor.TryJoin();

    // Создаем второй выходной поток для сравнения
    std::ostringstream new_result;
    new_doc.save(new_result);
    std::cout << "Comparison result: " << std::endl << "Documents are" << 
        (new_result.str() == result.str() ? " " : " not ") << "equal" << std::endl;
}
